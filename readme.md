## Repo structure

Copied files from CH is stored as it is, without renaming in the same folder structure.

**NB** do not hesitate to remove not needed references to components from the copied code. I have tried to copy all things and ends up with almost half of original project.

If you want to create new page:

1.  Create file under ``src/Pages/`` folder with ``.tsx`` extension
2. Create module file under ``src/Modules/`` folder with the ``.tsx`` extension
3. Import your page into new module 
``import {<Page>} from 'Pages/<page>'``
4. Define container where this page should be rendered. As you may know id on html page should be unique. So it is better to set container id to the page name.
5. Add new module as an entry point to ``webpack.config.js`` section ``entry``
6. Build project



## Commands
All comands stored in package.json ``sripts`` section
1. Run locally in dev mode: ``npm run start``
2. Build project: ``npm run build``
