const path = require("path");
var webpack = require("webpack");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

//const HtmlWebpackPlugin = require("html-webpack-plugin");
// const htmlWebpackPlugin = new HtmlWebpackPlugin({
//     template: path.join(__dirname, "./src/index.html"),
//     filename: "./index.html",
// });

//const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;


module.exports = {
    optimization: {
        minimize: true,
    },
    entry: {
        // apiKeysOperations: { import: path.join(__dirname, "src/Modules/ApiKeysOperationsModule.tsx") },
        // apiKeysGrid: { import: path.join(__dirname, "src/Modules/ApiKeysModule.tsx") },
        APIKeysDialog: { import: path.join(__dirname, "src/Modules/APIKeysDialogModule.tsx") },
        APIKeysPage: { import: path.join(__dirname, "src/Modules/APIKeysPageModule.tsx") },
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: "babel-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                loader: "ts-loader",
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        // fix "process is not defined" error:
        // (do "npm install process" before running the build)
        new webpack.ProvidePlugin({
            process: "process/browser",
        }),
        // htmlWebpackPlugin,
        // new BundleAnalyzerPlugin(),
    ],
    resolve: {
        extensions: [".ts", ".tsx", ".js", "jsx"],
        plugins: [new TsconfigPathsPlugin()],
        fallback: {
            fs: false,
            https: false,
        },
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "eval-cheap-module-source-map",

    devServer: {
        port: 3001,
    },
    output: {
        filename: "[name].js",
        chunkFilename: "[name].js",
        path: path.join(__dirname, "build"),
        hotUpdateMainFilename: "__hmr/[hash].hot-update.json",
        hotUpdateChunkFilename: "__hmr/[id].[hash].hot-update.js",
    },
};
