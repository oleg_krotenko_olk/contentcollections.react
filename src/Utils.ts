import { Nullable } from "@sitecore/sc-contenthub-webclient-sdk/dist/base-types";
import { IEntity } from "@sitecore/sc-contenthub-webclient-sdk/dist/contracts/base/entity";
import { IEntityDefinition } from "@sitecore/sc-contenthub-webclient-sdk/dist/contracts/base/entity-definition";
import { IMemberDefinition } from "@sitecore/sc-contenthub-webclient-sdk/dist/contracts/base/member-definition";
import { IRelationDefinition } from "@sitecore/sc-contenthub-webclient-sdk/dist/contracts/base/relation-definition";
import CultureInfo from "@sitecore/sc-contenthub-webclient-sdk/dist/culture-info";
import { ValidationError } from "@sitecore/sc-contenthub-webclient-sdk/dist/errors/validation-error";

import { KeyValuePair, LabelValuePair } from "Interfaces/ValuePairs";

export interface MapStringTo<T> {
    [key: string]: T;
}

declare global {
    interface Window {
        idStore: MapStringTo<number>;
    }
}

/**
 * https://stackoverflow.com/a/60206860
 * Returns an interface stripped of all keys that don't resolve to U, defaulting
 * to a non-strict comparison of T[key] extends U. Setting B to true performs
 * a strict type comparison of T[key] extends U & U extends T[key]
 */
export type KeysOfType<T, U, B = false> = {
    [P in keyof T]: B extends true
    ? T[P] extends U
    ? U extends T[P]
    ? P
    : never
    : never
    : T[P] extends U
    ? P
    : never;
}[keyof T];

export type PickByType<T, U, B = false> = Pick<T, KeysOfType<T, U, B>>;

// tslint:disable:no-null-keyword
export const isNullOrUndefined = (obj: unknown): obj is null | undefined => {
    return obj == null;
};

// Returns the value, optionally considering the passed culture and fallbacks.
export const getCultureValue = (
    value: any,
    culture: CultureInfo,
    fallbackCultures?: Array<CultureInfo | "*"> | CultureInfo | "*",
    fallbackValue?: any
): any => {
    if (value === null) {
        return null;
    }

    if (
        typeof value === "undefined" ||
        typeof value === "string" ||
        typeof value === "number" ||
        typeof value === "boolean"
    ) {
        return value;
    }

    if (!culture) {
        return value;
    }

    let cultureValue = value[culture];

    if (typeof cultureValue !== "undefined" || !(fallbackCultures || fallbackValue)) {
        return cultureValue;
    }

    let currentFallbackCultures: Array<CultureInfo | "*">;

    if (!(fallbackCultures instanceof Array)) {
        currentFallbackCultures = [fallbackCultures!];

        // Moved in the if function, not sure if it works
        currentFallbackCultures.forEach(fallback => {
            if (fallback === "*") {
                /* this means "just give me something... anything" and should be used with caution...
                it's useful when we want to show a list of stuff and we simply have no value for some of the items
                and we have some preferred fallbacks, but if it happens that neither of them is present, we just want
                a value for a random culture...
             */
                // for (const key in value) {
                //     cultureValue = value[key];
                //     return false; // break the each loop after first key
                // }

                //! Added
                // eslint-disable-next-line prefer-destructuring
                cultureValue = value[0];

                return false;
            }

            cultureValue = value[fallback];

            if (typeof cultureValue !== "undefined") {
                return false;
            }

            //! Added
            return true;
        });
    }

    if (typeof cultureValue === "undefined" && fallbackValue) {
        cultureValue = fallbackValue;
    }

    return cultureValue;
};

// generate a unique id
export const generateId = (prefix?: string, idLength?: number): string => {
    const chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const length = idLength || 5;
    let result = prefix || "";

    for (let i = length; i > 0; --i) {
        result += chars[Math.round(Math.random() * (chars.length - 1))];
    }

    return result;
};

export { v4 as generateGuid } from "uuid";

export const stringFormat = (format: string, values: Record<string, string>): string => {
    let currentFormat: string;

    if (!format) {
        return format;
    }

    currentFormat = typeof format !== "string" ? String(format) : format;

    if (!values) {
        return format;
    }

    Object.keys(values).forEach(key => {
        const value = values[key];

        currentFormat = currentFormat.replace(new RegExp(`{${key}}`, "g"), !isNullOrUndefined(value) ? value : "");
    });

    return currentFormat;
};

// map an array of objects to an object with a certain key
export const arrayToObject = (arr: Array<any>, keyField: string): object =>
    Object.assign({}, ...arr.map(item => ({ [item[keyField]]: item })));

export const hasPermission = (permissions: Array<string>, permission: string): boolean => {
    return permissions?.includes(permission.toLowerCase());
};

export const findTextTemplateVariables = (template: string, variableNamesOnly?: boolean): Array<string> => {
    // use regular expressions
    let localTemplateCopy = template;

    // match something like {...} where in between the {} we have at least 1 letter/number and we have ONLY letters and numbers
    const re0 = "(\\{([a-zA-Z0-9._:;][a-zA-Z0-9._:;]*)\\})";
    const p = new RegExp(re0, "i");
    const indices = [];

    let match = p.exec(localTemplateCopy);

    while (match) {
        indices.push(match[variableNamesOnly ? 2 : 1]);

        // replace the found group with {} so that we don't 'find' it again even if it occurs several times in the string
        localTemplateCopy = localTemplateCopy.replace(match[1], "{}");

        match = p.exec(localTemplateCopy);
    }

    return indices;
};

export const findEntityId = (url: string): Nullable<number> => {
    if (!url) {
        return null;
    }

    const decodedUrl = decodeURIComponent(url);
    const expression = /\/api\/entities\/([0-9]+)[?]?.*?/;
    const match = expression.exec(decodedUrl);

    return match && parseInt(match[1], 10);
};

export const getEntityDisplayName = (
    entity: IEntity,
    displayTemplate: string,
    culture: CultureInfo,
    definition: IEntityDefinition
): string => {
    let result: string;

    if (!entity) {
        throw new Error("Entity is required");
    }

    if (!displayTemplate) {
        return entity.id!.toString();
    }

    const templateVars = findTextTemplateVariables(displayTemplate);

    result = displayTemplate;
    templateVars.forEach((variable: string) => {
        const variableName = variable.replace("{", "").replace("}", "");
        let propertyValue;
        const { isMultiLanguage } = definition.getPropertyDefinition(variableName)!;

        if (isMultiLanguage) {
            propertyValue = entity.getPropertyValue(variableName, culture);
        } else {
            propertyValue = entity.getPropertyValue(variableName);
        }

        if (isNullOrUndefined(propertyValue)) {
            propertyValue = entity.id;
        }

        result = result.replace(variable, `${propertyValue}`);
    });

    return result;
};

/**
 * Get all the fields that a search request needs to load based on the display template
 *
 * @param displayTemplate - string
 */
export const getFieldsFromDisplayTemplate = (displayTemplate: string): Array<string> => {
    const templateVars = findTextTemplateVariables(displayTemplate);
    const fields = templateVars.map((variable: string) => {
        return variable.replace("{", "").replace("}", "");
    });

    return fields;
};

export const sanitizeElementId = (elementId: string): string => {
    return elementId.trim().replace(/ /g, "-");
};

window.idStore = window.idStore || {};

export const getAndUpdateElementIdCount = (elementId: string): string => {
    const id = sanitizeElementId(elementId);

    window.idStore[id] = window.idStore[id] + 1 || 0;

    return window.idStore[id].toString();
};

export const getUniqueElementId = (elementId: string): string => {
    const id = sanitizeElementId(elementId);
    const count = getAndUpdateElementIdCount(id);

    return id.length > 0 ? `${id}-${count}` : "";
};

export const getComponentId = (componentName: string, componentLabel = ""): string => {
    const componentNameWithoutSpaces = sanitizeElementId(componentName).toLocaleLowerCase();
    const labelWithoutSpaces = sanitizeElementId(componentLabel).toLocaleLowerCase();
    const separator = labelWithoutSpaces !== "" ? "-" : "";

    return getUniqueElementId(`${componentNameWithoutSpaces}${separator}${labelWithoutSpaces}`);
};

export type UniqueKeys<T> = Array<KeyValuePair<string, T>>;

export const getUniqueKeys = <T>(list: Array<T>, key: (item: T) => string): UniqueKeys<T> => {
    const uniqueProps: Array<string> = [];

    return list.map((item, index) => {
        const property = key(item);

        if (uniqueProps.find(prop => prop === property)) {
            return { key: `${property}-${index}`, value: item };
        }

        uniqueProps.push(property);

        return { key: property, value: item };
    });
};

export const hasOverflow = (element: HTMLElement): boolean => {
    if (element) {
        const el = element;

        return el.offsetHeight < el.scrollHeight || el.offsetWidth < el.scrollWidth;
    }

    return false;
};

export const scrollTopNot0 = (element?: HTMLElement): boolean => {
    if (element) {
        return hasOverflow(element) && element.scrollTop !== 0;
    }

    return true;
};

const getCurrentUrlParameters = (): URLSearchParams => new URLSearchParams(window.location.search);

/**
 * Add URL parameters.
 * @param parameters - Parameters to add
 * @param replaceState - Flag indicating if current state should be overwritten (default = false)
 */
export const addUrlParameters = (parameters: Record<string, string | number>, replaceState = false) => {
    if (parameters == null || Object.keys(parameters).length === 0) {
        return;
    }

    const urlParameters = getCurrentUrlParameters();

    Object.keys(parameters).forEach(key => {
        urlParameters.set(key, `${parameters[key]}`);
    });

    const newRelativePathQuery = `${window.location.pathname}?${urlParameters.toString()}`;

    if (replaceState) {
        window.history.replaceState(null, "", newRelativePathQuery);
    } else {
        window.history.pushState(null, "", newRelativePathQuery);
    }
};

/**
 * Add a parameter to the url.
 * @param name - The name of the parameter to add
 * @param value - The value of the parameter
 * @param replaceState - Flag indicating if current state should be overwritten (default = false)
 */
export const addUrlParam = (name: string, value: string | number, replaceState = false): void => {
    const params = getCurrentUrlParameters();

    params.set(name, `${value}`);

    const newRelativePathQuery = `${window.location.pathname}?${params.toString()}`;

    if (replaceState) {
        window.history.replaceState(null, "", newRelativePathQuery);
    } else {
        window.history.pushState(null, "", newRelativePathQuery);
    }
};

/**
 * Remove URL parameters.
 * @param parameters - The parameters to remove
 * @param replaceState - Flag indicating if current state should be overwritten (default = false)
 */
export const removeUrlParameters = (parameters: Array<string>, replaceState = false) => {
    if (parameters == null || Object.keys(parameters).length === 0) {
        return;
    }

    const urlParameters = getCurrentUrlParameters();

    parameters.forEach(param => {
        urlParameters.delete(param);
    });

    const newRelativePathQuery = `${window.location.pathname}?${urlParameters.toString()}`;

    if (replaceState) {
        window.history.replaceState(null, "", newRelativePathQuery);
    } else {
        window.history.pushState(null, "", newRelativePathQuery);
    }
};

/**
 * Remove a parameter from the url.
 * @param name - The name of the parameter to remove
 * @param replaceState - Flag indicating if current state should be overwritten (default = false)
 */
export const removeUrlParam = (name: string, replaceState = false): void => {
    const params = getCurrentUrlParameters();

    params.delete(name);

    const newRelativePathQuery = `${window.location.pathname}?${params.toString()}`;

    if (replaceState) {
        window.history.replaceState(null, "", newRelativePathQuery);
    } else {
        window.history.pushState(null, "", newRelativePathQuery);
    }
};

/**
 * Get the value of a parameter in the url.
 * @param name - The name of the parameter to retrieve
 */
export const getUrlParam = (name: string): Nullable<string> => {
    const params = new URLSearchParams(window.location.search);

    return params.get(name);
};

const isRelationDefinition = (item: unknown): item is IRelationDefinition => {
    return (item as IRelationDefinition).associatedEntityDefinitionName !== undefined;
};

/**
 * Get a technical label for entity definitions or member definitions.
 * The technical label is a concatenation of the definition label and the definition name.
 * If it's a self relation, the role will also be added to the label.
 * @param definition - The definition to generate a label for
 * @param culture - The culture in which to generate the label
 * @param definitionName - To check if a relation is a self relation
 * @remarks This function can generate a technical label for any interface that has a name and labels.
 */
export const getTechnicalLabel = (
    definition: IEntityDefinition | IMemberDefinition,
    culture: CultureInfo,
    definitionName?: string
): string => {
    const hasLabels = definition.labels && definition.labels[culture];
    let selfRelationLabel = "";

    if (isRelationDefinition(definition)) {
        if (
            definition.associatedEntityDefinitionName !== undefined &&
            definitionName !== undefined &&
            definition.associatedEntityDefinitionName === definitionName
        ) {
            selfRelationLabel = ` - ${definition.role}`;
        }
    }

    if (hasLabels && definition.labels[culture] !== definition.name) {
        return `${definition.labels[culture]}${selfRelationLabel} (${definition.name})`;
    }

    return `${definition.name}${selfRelationLabel}`;
};

/**
 * Find the translation in the messages bag for the given key.
 * @param messages - The list of translated messages
 * @param key - The key of the message to look for
 * @remarks For backwards compatibility, this function will try to find a message where:
 * - the key matches exactly
 * - the key is trimmed of all non-word and non-dash characters
 * - the key is trimmed of all spaces and pipe characters
 * @returns The translated message or the original key if no message was found
 */
export const getLocalizedLabel = (messages: Record<string, string>, key: string): string => {
    // /[\s|]/g = for old identifiers
    // /[\W_-]/g = for new identifiers
    // for >3.3.0 labels, the key is already sanitized
    const localizedLabel =
        messages &&
        key &&
        (messages[key] ||
            messages[key.replace(/[\W_-]/g, "")] ||
            messages[key.replace(/[\s|]/g, "")] ||
            messages[key.replace(/[\s|.]/g, "")]);

    return localizedLabel || key;
};

export const sortArray = <T>(array: Array<T>, sortOn: (item: T) => string): void => {
    array.sort((a: T, b: T) => {
        return sortOn(a) > sortOn(b) ? 1 : -1;
    });
};

export const arrayContainsArray = (superset: Array<string | number>, subset: Array<string | number>): boolean => {
    return subset.every((value: string | number) => {
        return superset.includes(value);
    });
};

export const hasPermissions = (permissionsArray: Array<string>, permissionsToLookFor: Array<string>): boolean => {
    return arrayContainsArray(permissionsArray, permissionsToLookFor);
};

export const stringToLabelValuePair = (items: Array<string>): Array<LabelValuePair<string>> => {
    return items.map(item => {
        const pair = {
            value: item,
            label: item,
        };

        return pair;
    });
};

export const isObject = (value: unknown): boolean => {
    return value !== null && typeof value === "object";
};

export const isEmptyObject = (obj: Record<string, unknown>): boolean => {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
};

export const isFunction = (f: unknown): f is Function => {
    return typeof f === "function";
};

/**
 * Format a number into a file size
 *
 * @param size - File size in bytes
 * @param messages - Messages object
 */
export const formatSize = (size: number, messages: Record<string, string>): string => {
    if (size > 100 * 1000 * 1000) {
        return (size / 1000000000).toFixed(2) + messages.GB;
    }

    if (size > 100 * 1000) {
        return (size / 1000000).toFixed(2) + messages.MB;
    }

    if (size > 100) {
        return (size / 1000).toFixed(2) + messages.KB;
    }

    return `${size}${messages.B}`;
};

export const twitterTime = (timeStamp: string): string => {
    const date = new Date(timeStamp);

    const diff = (new Date().getTime() - date.getTime()) / 1000;

    const dayDiff = Math.floor(diff / 86400);

    if (dayDiff < 0 || dayDiff >= 31) {
        return "";
    }

    if (dayDiff === 0) {
        if (diff < 5) {
            return "0s";
        }

        if (diff < 60) {
            return `${Math.floor(diff)}s`;
        }

        if (diff < 3600) {
            return `${Math.floor(diff / 60)}m`;
        }

        return `${Math.floor(diff / 3600)}h`;
    }

    if (dayDiff < 7) {
        return `${dayDiff}d`;
    }

    return `${Math.ceil(dayDiff / 7)}w`;
};

/**
 * A type guard that checks if a given key is present in the given object.
 */
// eslint-disable-next-line @typescript-eslint/ban-types
const has = <K extends string>(key: K, x: object): x is { [key in K]: unknown } => key in x;

export const getError = (
    error: unknown
): {
    errorMessage: string;
    stack?: string;
    failures?: Array<string>;
} => {
    let errorMessage = "";
    let stack = "";
    let failures: Array<string> = [];

    if (error !== undefined) {
        if (error instanceof ValidationError) {
            errorMessage = error.message;
            failures = error.failures.map(failure => failure.message || "");

            if (error.stack) {
                stack = error.stack;
            }
        } else if (typeof error === "string") {
            errorMessage = error;
        } else if (typeof error === "object" && error !== null) {
            if (has("message", error)) {
                errorMessage = error.message as string;

                if (has("stack", error)) {
                    stack = error.stack as string;
                }
            } else if (has("response", error) && error.response != null) {
                if (typeof error.response === "string") {
                    errorMessage = error.response;
                } else if (typeof error.response === "object") {
                    errorMessage = Object.keys(error.response || {}).join("\n");
                }
            } else if (has("reasonPhrase", error)) {
                if (Array.isArray(error.reasonPhrase)) {
                    errorMessage = error.reasonPhrase.join("\n");
                } else if (typeof error.reasonPhrase === "string") {
                    errorMessage = error.reasonPhrase;
                } else if (typeof error.reasonPhrase === "object") {
                    errorMessage = Object.values(error.reasonPhrase || {}).join("\n");
                }
            }
        } else {
            const message = typeof (error as any).toString === "function" && (error as any).toString();

            errorMessage = message || Object.prototype.toString.call(error);
        }
    }

    return {
        errorMessage,
        stack,
        failures,
    };
};

// get the taxonomy self relation
export const getTaxonomyToSelfRelation = (
    relations: Array<IRelationDefinition>,
    definitionName: string
): IRelationDefinition | undefined => {
    const relationToReturn = relations.find(relation => {
        return relation.associatedEntityDefinitionName === definitionName;
    });

    return relationToReturn;
};

export const sanitizeMemberName = (memberName: string): string => {
    return memberName.replace(/\./g, "_");
};

export const arrayHasValues = (arrayToCheck: Array<unknown> | undefined): boolean => {
    return Array.isArray(arrayToCheck) && arrayToCheck.length > 0;
};

export const isVerticallyOverflowing = (container: HTMLElement): boolean => {
    return container.scrollHeight > container.clientHeight;
};

export const getPaddingRight = (element: HTMLElement): number => {
    return parseInt(getComputedStyle(element).paddingRight, 10) || 0;
};

export const getScrollBarSize = (): number => {
    const scrollDiv = document.createElement("div");

    scrollDiv.style.width = "99px";
    scrollDiv.style.height = "99px";
    scrollDiv.style.position = "absolute";
    scrollDiv.style.top = "-9999px";
    scrollDiv.style.overflow = "scroll";

    document.body.appendChild(scrollDiv);

    const scrollBarSize = scrollDiv.offsetWidth - scrollDiv.clientWidth;

    document.body.removeChild(scrollDiv);

    return scrollBarSize;
};

// Main click action on a table row should not happen in some cases
export const preventClickEventCheck = (
    event: React.MouseEvent<HTMLAnchorElement | HTMLSpanElement, MouseEvent>
): boolean => {
    // Don't do the click when clicked on a dropdown item or on a operation
    return (
        !(event.target as HTMLElement).closest("[class*='makeStyles-searchTableRowOperations']") &&
        !(event.target as HTMLElement).closest(".MuiPopover-root")
    );
};

// Copy text to the clipboard
export const copyToClipboard = (text: string): void => {
    const dummy = document.createElement("textarea");

    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
};

export const elementIsVerticallyScrolled = (element: HTMLElement): boolean => {
    return element.scrollTop > 0;
};

declare global {
    interface Window { Page: any; }
}
window.Page = window.Page || {};

export const getEntityIdentifier = (): string => {
    return window.Page?.entity()?.data?.identifier || "";
}

export const notifyAPIKeyCreated = (): void => {
    window.Page.mediator.publish("api-key-created");
}

export const notifyAPIKeyDeleted = (apiKeysCount: number): void => {
    window.Page.mediator.publish("api-key-deleted", { count: apiKeysCount });
}

export const isOnAdminPage = (): boolean => {
    return window.Page?.name === "APIkeys";
}

// Copy text to the clipboard for modal dialog
// In modal we have to add dummy text node somewhere inside dialog markup.
// So, pass the ID of desired element
export const copyToClipboardModal = (text: string, parentId: string): void => {

    const dummy = document.createElement("textarea");

    const parentElement = document.getElementById(parentId);
    parentElement?.appendChild(dummy);
    dummy.value = text;

    dummy.select();
    document.execCommand("copy");
    parentElement?.removeChild(dummy);
};