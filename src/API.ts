
import { JsonObject } from "@sitecore/sc-contenthub-webclient-sdk/dist/base-types";
import { ContentHubClient } from "@sitecore/sc-contenthub-webclient-sdk/dist/clients/content-hub-client";
import { ResponseMessage } from "@sitecore/sc-contenthub-webclient-sdk/dist/clients/response-message";
import { ApiKeyDescription } from "Components/ApiKeyManagement/Interfaces/ApiKeyDescription";
import { ApiKeysPurpose } from "Components/ApiKeyManagement/Interfaces/ApiKeyPurpose";
import { getEntityIdentifier } from "Utils";

declare global {
    interface Window {
        apiKeyTranslations: Map<string, JsonObject | undefined>;
    }
}
window.apiKeyTranslations = window.apiKeyTranslations || new Map<string, JsonObject | undefined>();

export async function getLocalizeMessagesAsync(): Promise<ResponseMessage<JsonObject>> {
    const translations: JsonObject | undefined = window.apiKeyTranslations.get(window.Page.culture()) || undefined;
    if (translations) { return ResponseMessage.build(200, {}, { content: { values: translations } }) };
    const client = getContentHubClient();
    return client.raw.getAsync<JsonObject>(window.location.origin + `/api/resource?key=Components.EntityActions&culture=` + window.Page.culture())
        .then(response => {
            window.apiKeyTranslations.set(window.Page.culture(), response.content?.values as Record<string, string>)
            return response;
        });
}

export async function fetchApiKeysAsync(): Promise<ResponseMessage<Array<ApiKeyDescription>>> {
    const client = getContentHubClient();
    const identifier = getEntityIdentifier();
    const scopes = identifier ? `?scopes=${ApiKeysPurpose.Content}${identifier}` : '';
    return await client.raw.getAsync<Array<ApiKeyDescription>>(`${window.location.origin}/api/apikeys/listkeys${scopes}`);
}

export async function checkPermissionsAsync(): Promise<ResponseMessage<boolean>> {
    const client = getContentHubClient();
    return await client.raw.getAsync<boolean>(`${window.location.origin}/api/apikeys/hasmanagepermissions`);
}

export async function renameApiKeyAsync(hash: string, renameInfo: string): Promise<ResponseMessage> {
    const client = getContentHubClient();
    return await client.raw.putAsync(window.location.origin + `/api/apikeys/renamekey/${hash}`, renameInfo);
}

export async function createApiKeyAsync(apiKeyName: string, apiKeysPurpose: string): Promise<ResponseMessage> {
    const client = getContentHubClient();
    const identifier = getEntityIdentifier();
    const scopes = [apiKeysPurpose];
    if (identifier) { scopes.push(`${ApiKeysPurpose.Content}${identifier}`) }
    else { scopes.push(ApiKeysPurpose.ContentEverything) }
    return await client.raw.postAsync(window.location.origin + "/api/apikeys/createkey", {
        "label": apiKeyName,
        "scopes": scopes
    });
};


export async function revokeApiKeyAsync(hash: string): Promise<ResponseMessage> {
    const client = getContentHubClient();
    return await client.raw.putAsync(`${window.location.origin}/api/apikeys/revokekey/${hash}`, "");
}

function getContentHubClient(): ContentHubClient {
    const client = new ContentHubClient(window.location.origin);
    client.internalClient?.setRequestHeaders({
        "X-Requested-By": (document.getElementsByName("__RequestVerificationToken")[0] as HTMLInputElement).value,
    });
    return client;
}