import React, { createContext, ReactElement, ReactNode, useContext } from "react";

const MessagesContext = createContext({});

export interface MessagesContextProps {
    value?: Record<string, string>;
    children: ReactNode;
}

export function MessagesContextProvider({ value = {}, children }: MessagesContextProps): ReactElement {
    return <MessagesContext.Provider value={value}>{children}</MessagesContext.Provider>;
}

const useMessagesContext = (messagesToMerge?: Record<string, string>): Record<string, string> => {
    const context = useContext(MessagesContext);

    if (context === undefined) {
        throw new Error("useMessagesContext must be used within a MessagesContext.Provider");
    }

    return { ...messagesToMerge, ...context };
};

export default useMessagesContext;
