import React, {
    createContext,
    Dispatch,
    ReactElement,
    ReactNode,
    SetStateAction,
    useContext,
    useEffect,
    useMemo,
    useState,
} from "react";

import { Optional } from "@sitecore/sc-contenthub-webclient-sdk/dist/base-types";
import CultureInfo from "@sitecore/sc-contenthub-webclient-sdk/dist/culture-info";

import { PickByType } from "Utils";

/**
 * Properties for the options context provider.
 */
interface OptionsContextProviderProps {
    /**
     * Id of the entity in the current context.
     */
    entityId?: number;

    /**
     * Culture in the current context.
     */
    culture?: CultureInfo;

    /**
     * Indicates if the page is wrapped in a modal.
     */
    isInModal?: boolean;

    /**
     * Indicates if the page is edit mode.
     */
    isEditing?: boolean;

    /**
     * Indicates if the component is currently rendered inside a sidebar.
     */
    isInSidebar?: boolean;

    /**
     * Indicates if the component is currently rendered inside a tabs component.
     */
    isInTab?: boolean;
}

/**
 * Options context interface.
 */
export interface IOptionsContext extends Required<OptionsContextProviderProps> {
    /**
     * Method to set the entity id of the current context.
     */
    setEntityId: Dispatch<SetStateAction<number>>;

    /**
     * Method to set the culture of the current context.
     */
    setCulture: Dispatch<SetStateAction<CultureInfo>>;

    /**
     * Number representing the level of nesting (depth).
     * Can be seen as the number minus one of options contexts higher up the component tree.
     */
    nestingLevel: number;
}

const OptionsContext = createContext<Optional<IOptionsContext>>(undefined);

/**
 * The default options context for the most outer level.
 */
const defaultOptionsContext: IOptionsContext = {
    entityId: 0, // Entity id '0' does not exist in the system and can be used as falsy value.
    setEntityId: () => {},
    culture: "en-US", // The default culture.
    setCulture: () => {},
    isEditing: false,
    isInModal: false,
    isInTab: false,
    isInSidebar: false,
    nestingLevel: 0,
};

const useOptionsContext = (): IOptionsContext => {
    const context = useContext(OptionsContext);

    return context ?? defaultOptionsContext;
};

const getContextValue = function prepareValue<T>(
    value: T,
    parentValue: NonNullable<T>,
    nestingLevel: number
): { value: NonNullable<T>; setParentValue: boolean } {
    // When current value is null or undefined...
    if (value == null) {
        return {
            setParentValue: !!nestingLevel, // ...set the parent value when this value lives in a nested context.
            value: parentValue, // We always set the parent value because this ensures we take the defaults on level 1.
        };
    }

    return {
        setParentValue: false,
        value: value as NonNullable<T>,
    };
};

const useParentContextValue = function useParentContextValue<
    K extends keyof IOptionsContext,
    S extends keyof PickByType<IOptionsContext, Dispatch<SetStateAction<IOptionsContext[K]>>, true>
>(
    propValue: Optional<IOptionsContext[K]>,
    contextValueName: K,
    contextValueSetterName: S,
    defaultValue: IOptionsContext[K]
): [value: IOptionsContext[K], setValue: Dispatch<SetStateAction<IOptionsContext[K]>>] {
    const parentContext = useOptionsContext();

    const contextValue = getContextValue(
        propValue,
        parentContext[contextValueName] as NonNullable<IOptionsContext[K]>,
        parentContext.nestingLevel
    );

    const [value, setValue] = useState<IOptionsContext[K]>(contextValue.value ?? defaultValue);

    const valueSetter = useMemo(() => {
        return contextValue.setParentValue && contextValueSetterName
            ? (parentContext[contextValueSetterName] as Dispatch<SetStateAction<IOptionsContext[K]>>)
            : setValue;
    }, [contextValueSetterName, parentContext, contextValue.setParentValue]);

    useEffect(() => {
        // Always update local state.
        setValue(contextValue.value);
    }, [contextValue.value]);

    return [value, valueSetter];
};

/**
 * Provider for the options context.
 * @param props - Properties for the options context provider
 */
export function OptionsContextProvider({
    entityId: entityId_,
    culture: culture_,
    isEditing,
    isInModal,
    isInTab,
    isInSidebar,
    children,
}: OptionsContextProviderProps & { children: ReactNode }): ReactElement {
    const parentContext = useOptionsContext();

    const [entityId, setEntityId] = useParentContextValue(entityId_, "entityId", "setEntityId", 0);
    const [culture, setCulture] = useParentContextValue(culture_, "culture", "setCulture", "en-US");

    const optionsContext = useMemo(() => {
        return {
            entityId,
            setEntityId,
            culture,
            setCulture,
            isEditing: isEditing == null ? parentContext.isEditing : isEditing,
            isInModal: isInModal == null ? parentContext.isInModal : isInModal,
            isInTab: isInTab == null ? parentContext.isInTab : isInTab,
            isInSidebar: isInSidebar == null ? parentContext.isInSidebar : isInSidebar,
            nestingLevel: parentContext.nestingLevel + 1,
        } as IOptionsContext;
    }, [
        entityId,
        setEntityId,
        culture,
        setCulture,
        isEditing,
        parentContext.isEditing,
        parentContext.isInModal,
        parentContext.isInTab,
        parentContext.isInSidebar,
        parentContext.nestingLevel,
        isInModal,
        isInTab,
        isInSidebar,
    ]);

    return <OptionsContext.Provider value={optionsContext}>{children}</OptionsContext.Provider>;
}

export default OptionsContext;
export { OptionsContext, useOptionsContext };
