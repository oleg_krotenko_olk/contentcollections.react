import OAuthPasswordGrant from "@sitecore/sc-contenthub-webclient-sdk/dist/authentication/oauth-password-grant";
import { ContentHubClient } from "@sitecore/sc-contenthub-webclient-sdk/dist/clients/content-hub-client";

// const client = new ContentHubClient(window.location.origin);

// client.internalClient?.setRequestHeaders({
//     "X-Requested-By": (document.getElementsByName("__RequestVerificationToken")[0] as HTMLInputElement).value,
// });

// export default client;

// Your Sitecore Content Hub endpoint to connect to
const endpoint = "https://localhost:5001";

// Enter your credentials here
const oauth = new OAuthPasswordGrant("react", "react", "administrator", "admin");

// Create the JavaScript SDK client
const client = new ContentHubClient(endpoint, oauth);

client.internalClient?.setRequestHeaders({
    "X-Requested-By": (document.getElementsByName("__RequestVerificationToken")[0] as HTMLInputElement).value,
});

export default client;
