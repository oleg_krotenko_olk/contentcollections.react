import React from "react";
import { render } from 'react-dom';
import { create } from "jss";
import rtl from "jss-rtl";
import { createGenerateClassName, jssPreset, StylesProvider, ThemeProvider } from "@material-ui/core";
import { SnackbarProvider } from 'notistack';
import { DialogWrapper } from "./APIKeysDialog";
import theme from 'ContentHubThemes';

// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

const generateClassName = createGenerateClassName({
    disableGlobal: true, // Minify css class names
    productionPrefix: "chapikeysperations", // 'jss' by default
});

const wrapper = document.getElementById("api-keys-container");
render(
    <StylesProvider jss={jss} generateClassName={generateClassName}>
        <SnackbarProvider maxSnack={3} anchorOrigin={{ vertical: 'bottom', horizontal: 'right', }}>
            <ThemeProvider theme={theme}>
                <DialogWrapper />
            </ThemeProvider>
        </SnackbarProvider>
    </StylesProvider>,
    wrapper);