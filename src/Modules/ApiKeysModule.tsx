// import React from "react";
// import { render } from 'react-dom';
// import { create } from "jss";
// import rtl from "jss-rtl";
// import ApiKeysComponent from "Components/ApiKeyManagement/ApiKeysComponent";
// import { createGenerateClassName, jssPreset, StylesProvider } from "@material-ui/core";

// // Configure JSS
// const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

// const generateClassName = createGenerateClassName({
//     disableGlobal: true, // Don't minify css class names
//     productionPrefix: "chapikeys", // 'jss' by default
// });

// const wrapper = document.getElementById("api-keys-grid-container");
// render(
//     <StylesProvider jss={jss} generateClassName={generateClassName}>
//         <ApiKeysComponent />
//     </StylesProvider>,
//     wrapper);