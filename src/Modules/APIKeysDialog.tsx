import AddApiKeyComponent from 'Components/ApiKeyManagement/AddApiKeyComponent/AddApiKeyComponent';
import ApiKeysComponent from 'Components/ApiKeyManagement/ApiKeysComponent';
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core';
import { getLocalizeMessagesAsync, fetchApiKeysAsync, revokeApiKeyAsync, renameApiKeyAsync, checkPermissionsAsync, createApiKeyAsync } from 'API';
import { ResponseMessage } from '@sitecore/sc-contenthub-webclient-sdk/dist/clients/response-message';
import { ApiKeyDescription } from 'Components/ApiKeyManagement/Interfaces/ApiKeyDescription';
import { Nullable } from '@sitecore/sc-contenthub-webclient-sdk/dist/base-types';
import useConfirmation from 'Components/CustomHooks/useConfirmation';
import { notifyAPIKeyCreated } from 'Utils';
import { RenameApiKeyInfo } from 'Components/ApiKeyManagement/Interfaces/RenameApiKeyInfo';
import { defaultMessages } from 'defaultMessages';
import { NotificationDialog } from 'Components/ApiKeyManagement/notification.dialog';

const useStyles = makeStyles(({
    addContainer: {
        display: 'flex',
        flexWrap: 'nowrap',
        justifyContent: 'flex-end'
    },
    gridContainer: {
        display: 'flex',
        flexWrap: 'nowrap',
        justifyContent: 'center'
    }
}))

export function DialogWrapper() {
    const classes = useStyles();
    const [messages, setMessage] = useState(defaultMessages);
    const [initialized, setInitialized] = useState<boolean>(false);
    const [accessForbidden, setAccessForbidden] = useState(false);
    const [keysData, setKeysData] = useState<Array<ApiKeyDescription>>([]);
    const [allowedManageKeys, setAllowedManageKeys] = useState(false);
    const [confirm] = useConfirmation();

    useEffect(() => {
        Promise.all([
            getLocalizeMessagesAsync(),
            fetchApiKeysAsync(),
            checkPermissionsAsync()
        ])
            .then(([messages, apiKeys, permissions]) => {
                setMessage(messages.content?.values as Record<string, string>);
                parseAPIKeysResponse(apiKeys);
                const content = permissions.isSuccessStatusCode ? permissions.content ?? false : false;
                setAllowedManageKeys(content);

                setInitialized(true);
            });
    }, [])

    const onAddApiKey = () => {
        void fetchApiKeysAsync().then(response => parseAPIKeysResponse(response));
    }

    const parseAPIKeysResponse = (keysResponse: ResponseMessage<Array<ApiKeyDescription>>) => {
        if (keysResponse.isSuccessStatusCode) {
            const content = keysResponse.content ?? [];
            setKeysData(content);
        }
        else if (keysResponse.statusCode === 403 || keysResponse.statusCode === 204) {
            setAccessForbidden(true);
        }
        else if (keysResponse.statusCode === 404) {
            setAccessForbidden(true);
            console.warn("api/apikeys/listkeys was not found. Possible reason - publishing is disabled");
        }
        else {
            // report the issue..
        }
    }

    const onDeleteKey = (item: ApiKeyDescription): void => {
        confirm(messages.confirmDeleteApiKey, messages.confirmDeleteApiKeyTitle, messages.delete, messages.cancel)
            .then(
                async (): Promise<void> => {
                    const response = await revokeApiKeyAsync(item.hash);
                    if (response.isSuccessStatusCode) {
                        await fetchApiKeysAsync().then(response => parseAPIKeysResponse(response));
                        notifyAPIKeyCreated();
                    }
                    else {
                        // report the issue..
                    }
                }
            )
            .catch(() => { });
    };

    const onSaveRenamedKey = async (name: string, selectedKey: Nullable<ApiKeyDescription>) => {
        if (!selectedKey) {
            // report the issue..
            return;
        }

        const renameInfo: RenameApiKeyInfo = { newName: name };
        const response = await renameApiKeyAsync(selectedKey?.hash, JSON.stringify(renameInfo));
        if (response.isSuccessStatusCode) {
            let clonedKeysData = [...keysData];
            const index = clonedKeysData.findIndex(key => key.hash === selectedKey?.hash);
            if (index !== -1) {
                clonedKeysData[index].label = name;
                setKeysData(clonedKeysData);
            } else {
                // report issue - changed key is not found in the keys collection!
            }
        }
        else {
            // report the issue..
        }
    }

    return (
        <>
            {
                initialized ? (
                    accessForbidden ? (<NotificationDialog messages={messages} showModal={true} onCloseModal={() => { console.log("closed") }} />) :
                        <>
                            <div className={classes.addContainer}>
                                <AddApiKeyComponent
                                    messages={messages}
                                    onAddApiKey={onAddApiKey}
                                    allowedManageKeys={allowedManageKeys}
                                />
                            </div>
                            <div className={classes.gridContainer}>
                                <ApiKeysComponent
                                    messages={messages}
                                    keysData={keysData}
                                    onDeleteKey={onDeleteKey}
                                    onSaveRenamedKey={onSaveRenamedKey}
                                    accessForbidden={accessForbidden} />
                            </div>
                        </>
                ) : <></>
            }
        </>
    )
}