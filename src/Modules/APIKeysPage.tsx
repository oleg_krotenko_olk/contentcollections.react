// combine class names
import classNames from "classnames";
import React, { FunctionComponent, ReactElement, useEffect, useState } from "react";
import { Box, Grid, makeStyles, Typography, useTheme } from "@material-ui/core";
import {
    grayColorTransparent,
    navbarBg,
    navbarHeight,
    pageHeaderBg,
    pageHeaderColor,
    pageHeaderDarkMode,
    pageHeaderFont,
    pageHeaderFontSize,
    pageHeaderFontWeight,
    pageHeaderHeight,
    pageHeaderTextTransform,
    whiteColorTransparent,
} from "ThemeVariables";
import { getLocalizeMessagesAsync, fetchApiKeysAsync, revokeApiKeyAsync, renameApiKeyAsync, checkPermissionsAsync, createApiKeyAsync } from 'API';
import { ResponseMessage } from '@sitecore/sc-contenthub-webclient-sdk/dist/clients/response-message';
import { ApiKeyDescription } from 'Components/ApiKeyManagement/Interfaces/ApiKeyDescription';
import { Nullable } from '@sitecore/sc-contenthub-webclient-sdk/dist/base-types';
import useConfirmation from 'Components/CustomHooks/useConfirmation';
import { notifyAPIKeyCreated } from 'Utils';
import { RenameApiKeyInfo } from 'Components/ApiKeyManagement/Interfaces/RenameApiKeyInfo';
import ApiKeysComponent from 'Components/ApiKeyManagement/ApiKeysComponent';
import AddApiKeyComponent from "Components/ApiKeyManagement/AddApiKeyComponent/AddApiKeyComponent";
import { defaultMessages } from 'defaultMessages';
import { NotificationDialog } from "Components/ApiKeyManagement/notification.dialog";

let borderTop: string;
if (pageHeaderBg === navbarBg && pageHeaderDarkMode) {
    borderTop = `1px solid ${whiteColorTransparent["400"]}`;
} else if (pageHeaderBg === navbarBg && !pageHeaderDarkMode) {
    borderTop = `1px solid ${grayColorTransparent["300"]}`;
} else {
    borderTop = "none";
}
// styles
const useStyles = makeStyles(theme => ({
    pageHeaderWrapper: {
        position: "fixed",
        top: `${navbarHeight}px`,
        backgroundColor: pageHeaderBg,
        color: pageHeaderColor,
        fontWeight: pageHeaderFontWeight as number,
        fontFamily: pageHeaderFont,
        paddingLeft: theme.spacing(3),
        paddingRight: theme.spacing(3),
        overflowY: "hidden",
        overflowX: "auto",
        lineHeight: 1.3,
        minHeight: pageHeaderHeight,
        left: 0,
        right: 0,
        borderTop,
        boxShadow: theme.shadows[3],
        zIndex: theme.zIndex.appBar,

        [theme.breakpoints.down("sm")]: {
            paddingLeft: theme.spacing(2),
            paddingRight: theme.spacing(2),
        },
    },
    main: {
        paddingTop: pageHeaderHeight
    },
    backButtonWrapper: {},
    headerZoneLeft: {
        flex: 1,
        minWidth: "36px",
        overflow: "hidden",
        marginRight: theme.spacing(1),
        maxWidth: "100%",
        display: "flex",
        alignItems: "center",
        flexWrap: "nowrap",
    },
    headerZoneRight: {
        "display": "flex",
        "alignItems": "center",
        "flexWrap": "nowrap",
        "& [data-style-name='indicatorsWrapper']": {
            "marginBottom": 0,
            "& > *": {
                marginBottom: 0,
            },
        },
        "& > *": {
            marginLeft: theme.spacing(1),
        },
    },
    pageTitle: {
        margin: "0 !important",
        fontSize: pageHeaderFontSize,
        color: pageHeaderColor,
        fontFamily: pageHeaderFont,
        fontWeight: pageHeaderFontWeight as number,
        textTransform: pageHeaderTextTransform as "uppercase" | "none",
        whiteSpace: "nowrap",
    },
}));

// component
const FixedPageHeader: FunctionComponent = (): ReactElement => {
    const theme = useTheme();
    const classes = useStyles(theme);
    const [messages, setMessage] = useState(defaultMessages);
    const [initialized, setInitialized] = useState<boolean>(false);
    const [accessForbidden, setAccessForbidden] = useState(false);
    const [keysData, setKeysData] = useState<Array<ApiKeyDescription>>([]);
    const [allowedManageKeys, setAllowedManageKeys] = useState(false);
    const [confirm] = useConfirmation();
    const [modalVisible, setModalVisible] = useState<boolean>(false);

    useEffect(() => {
        Promise.all([
            getLocalizeMessagesAsync(),
            fetchApiKeysAsync(),
            checkPermissionsAsync()
        ])
            .then(([messages, apiKeys, permissions]) => {
                setMessage(messages.content?.values as Record<string, string>);
                parseAPIKeysResponse(apiKeys);
                const content = permissions.isSuccessStatusCode ? permissions.content ?? false : false;
                setAllowedManageKeys(content);

                setInitialized(true);
            });
    }, [])

    const parseAPIKeysResponse = (keysResponse: ResponseMessage<Array<ApiKeyDescription>>) => {
        if (keysResponse.isSuccessStatusCode) {
            const content = keysResponse.content ?? [];
            setKeysData(content);
        }
        else if (keysResponse.statusCode === 403 || keysResponse.statusCode === 204) {
            setAccessForbidden(true);
            setModalVisible(true);
        }
        else if (keysResponse.statusCode === 404) {
            setAccessForbidden(true);
            setModalVisible(true);
            console.warn("api/apikeys/listkeys was not found. Possible reason - publishing is disabled");
        }
        else {
            // report the issue..
        }
    }
    const onAddApiKey = () => {
        void fetchApiKeysAsync().then(response => parseAPIKeysResponse(response));
    }

    const onDeleteKey = (item: ApiKeyDescription): void => {
        confirm(messages.confirmDeleteApiKey, messages.confirmDeleteApiKeyTitle, messages.delete, messages.cancel)
            .then(
                async (): Promise<void> => {
                    const response = await revokeApiKeyAsync(item.hash);
                    if (response.isSuccessStatusCode) {
                        await fetchApiKeysAsync().then(response => parseAPIKeysResponse(response));
                        notifyAPIKeyCreated();
                    }
                    else {
                        // report the issue..
                    }
                }
            )
            .catch(() => { });
    };

    const onSaveRenamedKey = async (name: string, selectedKey: Nullable<ApiKeyDescription>) => {
        if (!selectedKey) {
            // report the issue..
            return;
        }

        const renameInfo: RenameApiKeyInfo = { newName: name };
        const response = await renameApiKeyAsync(selectedKey?.hash, JSON.stringify(renameInfo));
        if (response.isSuccessStatusCode) {
            let clonedKeysData = [...keysData];
            const index = clonedKeysData.findIndex(key => key.hash === selectedKey?.hash);
            if (index !== -1) {
                clonedKeysData[index].label = name;
                setKeysData(clonedKeysData);
            } else {
                // report issue - changed key is not found in the keys collection!
            }
        }
        else {
            // report the issue..
        }
    }


    return (
        <React.Fragment>
            <Grid
                container
                alignItems="center"
                className={classNames(classes.pageHeaderWrapper, "mui-fixed")}
                wrap="nowrap"
                role="complementary"
            >
                <Box className={classes.headerZoneLeft}>
                    <Typography variant="h2" component="h2" className={classes.pageTitle}>
                        API keys
                </Typography>
                </Box>
                <Box className={classes.headerZoneRight}>
                    {
                        initialized ? <AddApiKeyComponent
                            messages={messages}
                            onAddApiKey={onAddApiKey}
                            allowedManageKeys={allowedManageKeys}
                        /> : <></>
                    }

                </Box>


            </Grid>
            <Box className={classes.main}>
                {initialized ?
                    <ApiKeysComponent
                        messages={messages}
                        keysData={keysData}
                        onDeleteKey={onDeleteKey}
                        onSaveRenamedKey={onSaveRenamedKey}
                        accessForbidden={accessForbidden} />
                    : <></>
                }
            </Box>

            {initialized && accessForbidden ? (
                <NotificationDialog
                    messages={messages}
                    showModal={modalVisible}
                    onCloseModal={() => { setModalVisible(false); window.history.back(); }} />) :
                <></>}
        </React.Fragment>
    );
};
export default FixedPageHeader;