// import React from "react";
// import { render } from 'react-dom';
// import { create } from "jss";
// import rtl from "jss-rtl";
// import AddApiKeyComponent from "Components/ApiKeyManagement/AddApiKeyComponent/AddApiKeyComponent";
// import { createGenerateClassName, jssPreset, StylesProvider } from "@material-ui/core";
// import { SnackbarProvider } from 'notistack';

// // Configure JSS
// const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

// const generateClassName = createGenerateClassName({
//     disableGlobal: true, // Don't minify css class names
//     productionPrefix: "chapikeysperations", // 'jss' by default
// });

// const wrapper = document.getElementById("api-keys-operations-container");
// render(
//     <StylesProvider jss={jss} generateClassName={generateClassName}>
//         <SnackbarProvider maxSnack={3} anchorOrigin={{ vertical: 'bottom', horizontal: 'right', }}>
//             <AddApiKeyComponent messages={{}} />
//         </SnackbarProvider>
//     </StylesProvider>,
//     wrapper);