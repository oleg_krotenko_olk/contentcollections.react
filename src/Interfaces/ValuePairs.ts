export interface LabelValuePair<T> {
    value: T;
    label: string;
}

export interface KeyValuePair<T, K> {
    key: T;
    value: K;
}
