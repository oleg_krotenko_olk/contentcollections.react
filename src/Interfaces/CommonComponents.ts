export interface ConfirmationConfig {
    okLabel: string;
    cancelLabel: string;
    confirmTitle: string;
    confirmMessage: string;
}

export type ColorName =
    | "white"
    | "black"
    | "red"
    | "pink"
    | "purple"
    | "deep-purple"
    | "indigo"
    | "blue"
    | "light-blue"
    | "cyan"
    | "teal"
    | "green"
    | "light-green"
    | "lime"
    | "yellow"
    | "amber"
    | "orange"
    | "deep-orange"
    | "brown"
    | "gray"
    | "light-gray"
    | "theme-color";
