import { Container, Typography } from "@material-ui/core";
import Modal from "Components/Common/Modal/Modal";
import ModalOperation from "Components/Common/Operations/OpenModal/ModalOperation";
import { defaultMessages } from "defaultMessages";
import React, { FunctionComponent, ReactElement } from "react";

interface NotificationDialogProps {
    messages: Record<string, string>,
    showModal: boolean;
    onCloseModal: () => void;
}

export const NotificationDialog: FunctionComponent<NotificationDialogProps> = ({
    messages,
    showModal,
    onCloseModal
}): ReactElement => {
    return (
        <Modal
            modalTitle={messages.couldNotLoadApiKeysTitle ?? defaultMessages.couldNotLoadApiKeysTitle}
            modalVisible={showModal}
            onModalClose={onCloseModal}
            id={`created-key-warning`}
            maxWidth="sm"
            modalType="SmallModal"
            modalActions={[
                <ModalOperation
                    closeModal={onCloseModal}
                    displayMode="label"
                    label={"OK"}
                    key="create_parent"
                    variant="contained"
                    color="primary"
                    id="create"
                />
            ]}
        >
            <Container maxWidth="md">
                <Typography>
                    {messages.couldNotLoadApiKeys ?? defaultMessages.couldNotLoadApiKeys}
                </Typography>
            </Container>
        </Modal>
    )
}