import React, { FunctionComponent, ReactElement } from 'react';
import Theme from 'ContentHubThemes';
import {
    makeStyles,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from '@material-ui/core';
import { textColorLight, textColorLightOnDark } from 'ThemeVariables';
import DataGridRow from 'Components/Common/DataGridRow/DataGridRow';
import Field, { FieldProps } from 'Components/Common/Fields/Field';
import Operation, { OperationProps } from 'Components/Common/Operations/Operation';
import NoResultsText from "Components/Common/NoResultsText";
import { ApiKeyDescription } from 'Components/ApiKeyManagement/Interfaces/ApiKeyDescription';
import { format } from 'date-fns';
import { ApiKeysPurpose } from 'Components/ApiKeyManagement/Interfaces/ApiKeyPurpose';
import { isOnAdminPage } from 'Utils';

// styles
const useStyles = makeStyles(theme => ({
    root: {
        width: "100%",
        overflow: "auto",
        minHeight: "40px",
        boxShadow: "none",
    },
    noResults: {
        textAlign: "center",
        padding: theme.spacing(4),
        color: theme.palette.type === "dark" ? textColorLightOnDark : textColorLight,
    },
}));

interface ApiKeysProps {
    messages: Record<string, string>,
    keysData: Array<ApiKeyDescription>,
    onDeleteKey: (data: ApiKeyDescription) => void;
    onRenameKey: (data: ApiKeyDescription) => void;
}

const ApiKeysGrid: FunctionComponent<ApiKeysProps> = ({
    messages,
    keysData,
    onDeleteKey,
    onRenameKey
}): ReactElement => {
    const classes = useStyles(Theme);
    const getKeyScope = (scopes: string[]) => {
        return scopes.some(x => x === ApiKeysPurpose.Delivery) ? messages.delivery : messages.preview;
    }

    const tableRows = keysData.map((item, i) => {
        const fields: Array<ReactElement<FieldProps>> = [
            <Field key={item.label} label={item.label} />,
            <Field key={i + 'item.scopes'} label={getKeyScope(item.scopes)} />,
            <Field key={`${item.label}-created-${item.created}`} label={format(new Date(item.created), 'MMM dd, yyyy')} />
        ];

        if (isOnAdminPage()) {
            fields.splice(fields.length - 1, 0, <Field key={i + 'item.createdBy'} label={item.createdBy} />)
        }

        const operations: Array<ReactElement<OperationProps>> = [
            <Operation
                onClick={e => {
                    e.stopPropagation();
                    onRenameKey(item);
                }}
                displayMode="icon"
                label={messages.rename}
                icon="m-icon-pen"
                id={`${item.label}-rename`}
                key={`${item.label}-rename`}
            />,
            <Operation
                onClick={e => {
                    e.stopPropagation();
                    onDeleteKey(item);
                }}
                displayMode="icon"
                label={messages.delete}
                icon="m-icon-trash"
                id={`${item.label}-delete`}
                key={`${item.label}-delete`}
            />
        ];

        return (
            <DataGridRow
                key={item.label}
                fields={fields}
                operations={operations}
                id={`${item.label}`}
            />
        );
    });

    return (
        <Paper className={classes.root}>
            {keysData.length === 0 ? (
                <NoResultsText message={messages.noResults} />
            ) : (
                    <Table id="apiKeys">
                        <TableHead>
                            <TableRow>
                                <TableCell>{messages.name}</TableCell>
                                <TableCell>{messages.purpose}</TableCell>
                                {
                                    isOnAdminPage() ? (
                                        <TableCell>{messages.createdBy}</TableCell>
                                    ) : null
                                }
                                <TableCell>{messages.dateCreated}</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>{tableRows}</TableBody>
                    </Table>
                )}
        </Paper>
    );
}

export default ApiKeysGrid;
