import Label from "Components/Common/Editors/Label";
import TextEditor from "Components/Common/Editors/TextEditor/TextEditor";
import Modal from "Components/Common/Modal/Modal";
import Operation from "Components/Common/Operations/Operation";
import Theme from "ContentHubThemes";
import { makeStyles } from "@material-ui/core";
import React, { ReactText } from "react";
import { FunctionComponent, ReactElement } from "react";

const useStyles = makeStyles(() => ({
  noMarginTop: {
    marginTop: 0,
  },
}));

interface ApiKeyDetailsProps {
  messages: Record<string, string>,
  showModal: boolean;
  keyName: string;
  onCloseModal: () => void;
  onSaveRenamedKey: (name: string) => void;
}

const ApiKeyRenameModal: FunctionComponent<ApiKeyDetailsProps> = ({
  messages,
  showModal,
  keyName,
  onCloseModal,
  onSaveRenamedKey
}): ReactElement => {

  const classes = useStyles(Theme);
  const identifierMaxLength = 50;

  var newName: string;

  const onSave = async (): Promise<void> => {
    if (newName && keyName != newName.trim()) {
      onSaveRenamedKey(newName);
    }
    onCloseModal();
  };

  const onKeyChange = (value: ReactText): void => {
    if (typeof value === 'string') {
      if (value.length <= identifierMaxLength || value === '') {
        newName = value;
      }
    }
  };

  const modalActions = [
    <Operation
      onClick={() => {
        onSave();
      }}
      displayMode="label"
      label={messages.save}
      key="save"
      variant="contained"
      color="primary"
      id={`${keyName}-save`}
    />,
    <Operation
      onClick={onCloseModal}
      displayMode="label"
      label={messages.cancel}
      key="cancel"
      variant="text"
      color="primary"
      id={`${keyName}-cancel`}
    />,
  ];


  return (
    <Modal
        modalTitle={messages.renameKey}
      modalVisible={showModal}
      onModalClose={onCloseModal}
      modalActions={modalActions}
      id={`${keyName}-rename-modal`}
    >
      <Label label={messages.name} isRequired>
        <TextEditor
          isRequired={true}
          onChange={onKeyChange}
          value={keyName}
          className={classes.noMarginTop}
          autoFocus={false}
          errorMessage={messages.required}
          id="keyName"
        />
      </Label>
    </Modal>
  );
}

export default ApiKeyRenameModal;