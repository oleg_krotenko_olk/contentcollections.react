export interface ApiKeyDescription {
    label: string;
    scopes: string[];
    createdBy: string;
    hash: string;
    created: Date;
}