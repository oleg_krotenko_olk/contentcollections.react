export const enum ApiKeysPurpose {
    Delivery = "audience-delivery",
    Preview = "audience-preview",
    Content = 'content-',
    ContentEverything = 'content-#everything#'
}
