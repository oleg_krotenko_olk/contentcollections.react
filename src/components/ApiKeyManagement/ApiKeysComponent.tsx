import React, { FunctionComponent, ReactElement, useState } from 'react';
import ApiKeysGrid from "Components/ApiKeyManagement/ApiKeysGrid";
import { ApiKeyDescription } from 'Components/ApiKeyManagement/Interfaces/ApiKeyDescription';
import ApiKeyRenameModal from "Components/ApiKeyManagement/ApiKeyRenameModal"
import { Nullable } from '@sitecore/sc-contenthub-webclient-sdk/dist/base-types';

interface ApiKeysComponentProps {
    keysData: Array<ApiKeyDescription>,
    messages: Record<string, string>,
    onDeleteKey: (data: ApiKeyDescription) => void;
    onSaveRenamedKey: (name: string, selectedKey: Nullable<ApiKeyDescription>) => void,
    accessForbidden: boolean
}

const ApiKeysComponent: FunctionComponent<ApiKeysComponentProps> = ({
    keysData,
    messages,
    onDeleteKey,
    onSaveRenamedKey,
    accessForbidden }): ReactElement => {
    const [showModal, setShowModal] = useState(false);
    const [selectedKey, setSelectedKey] = useState<Nullable<ApiKeyDescription>>(null);
    
    const onRenameKey = (item: ApiKeyDescription): void => {
        setSelectedKey(item);
        setShowModal(true);
    };

    const onCloseModal = (): void => {
        setShowModal(false);
        setSelectedKey(null);
    };

    return (
        <>
            {accessForbidden ? null :
                <ApiKeysGrid
                    messages={messages}
                    keysData={keysData}
                    onDeleteKey={onDeleteKey}
                    onRenameKey={onRenameKey}
                />
            }

            <ApiKeyRenameModal
                messages={messages}
                showModal={selectedKey !== null && showModal}
                keyName={selectedKey !== null ? selectedKey?.label : ""}
                onCloseModal={onCloseModal}
                onSaveRenamedKey={(name) => onSaveRenamedKey(name, selectedKey)}
            />
        </>
    );
}
export default ApiKeysComponent;