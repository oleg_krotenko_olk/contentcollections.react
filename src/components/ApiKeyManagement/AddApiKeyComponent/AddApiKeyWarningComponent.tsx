import TextEditor from 'Components/Common/Editors/TextEditor/TextEditor';
import Modal from 'Components/Common/Modal/Modal';
import Operation from 'Components/Common/Operations/Operation';
import React, { FunctionComponent, ReactElement } from 'react';
import { copyToClipboardModal } from 'Utils';
import { Grid, makeStyles } from '@material-ui/core';
import { Alert } from "@material-ui/lab";
import { useSnackbar } from 'notistack';
import classNames from 'classnames';

const useStyle = makeStyles(() => ({
  textEditor: {
    flexGrow: 1,
  },
  alertText: {
    fontSize: '14px !important',
    fontWeight: 400,
  }
}));

interface ApiKeyWarningProps {
  messages: Record<string, string>;
  keyCode: string;
  showModal: boolean;
  onCloseModal: () => void;
}

const AddApiKeyWarningComponent: FunctionComponent<ApiKeyWarningProps> = ({
  messages,
  keyCode,
  showModal,
  onCloseModal
}): ReactElement => {
  const alertWarningClassName = 'alert alert-warning';
  const alertWarningIconClassName = 'm-icon m-icon-alert';
  const { enqueueSnackbar } = useSnackbar();

  const domId = 'clipboard-copy-parent-id';
  const onShare = (): void => {
    copyToClipboardModal(keyCode, domId);
    enqueueSnackbar(messages.copiedToClipboard, { variant: 'success' });
  };
  const classes = useStyle();
  return (
    <Modal
      modalTitle={messages.apiKeyCreated}
      modalVisible={showModal}
      onModalClose={onCloseModal}
      id={`created-key-warning`}
      fullWidth={true}
      maxWidth="sm"
      modalType="SmallModal"
      modalActions={[
        <React.Fragment>
          <p>&nbsp;</p>
        </React.Fragment>
      ]}
    >
      <Grid id={domId} container spacing={1} >
        <Grid item xs={12}>
          <Alert
            icon={<span className={alertWarningIconClassName} />}
            severity="warning"
            className={classNames([alertWarningClassName, classes.alertText])}>
            {messages.apiKeyVisibleWarning}
          </Alert>
        </Grid>
        <Grid item className={classes.textEditor}>
          <TextEditor
            isRequired
            value={keyCode}
            fullWidth
            className={classes.textEditor}
            disabled
            maxWidth="none"
            id={domId}
          />
        </Grid>
        <Grid item>
          <Operation
            onClick={onShare}
            icon={'copy'}
            label={messages.copy}
            id={'copy-to-clipboard-operation'}
            displayMode="iconLabel"
            displayType={'secondary'}
            variant="contained"
          />
        </Grid>
      </Grid>
    </Modal>
  );
};

export default AddApiKeyWarningComponent;