import React, { ReactElement, ReactText, useState } from "react";
import ModalOperation from "Components/Common/Operations/OpenModal/ModalOperation";
import OpenModalOperation from "Components/Common/Operations/OpenModal/OpenModalOperation";
import Label from "Components/Common/Editors/Label";
import TextEditor from "Components/Common/Editors/TextEditor/TextEditor";
import RadioButtonEditor from "Components/Common/Editors/RadioButtonEditor/RadioButtonEditor";
import { LabelValuePair } from "Interfaces/ValuePairs";
import { Container } from "@material-ui/core";
import { ApiKeysPurpose } from 'Components/ApiKeyManagement/Interfaces/ApiKeyPurpose';

interface AddApiKeyOperationsComponentProps {
  messages: Record<string, string>;
  onSave: (name: string, purpose: string) => Promise<boolean>;
}

const AddApiKeyOperationsComponent: React.FunctionComponent<AddApiKeyOperationsComponentProps> = ({
  messages,
  onSave
}): ReactElement => {
  const [name, setName] = useState("");
  const [apiKeysPurpose, setApiKeysPurposes] = useState<ApiKeysPurpose>(ApiKeysPurpose.Delivery);
  const regex = /^.{0,75}/; // any symbols allowed

  const initializeModal = (): Promise<void> => {
    setName("");
    return Promise.resolve();
  };

  const onNameChanged = (newName: ReactText): void => {
    if (typeof newName === "string") {
      setName(newName.trim());
    }
  };

  const onPurposeChanged = (type: string): void => {
    setApiKeysPurposes(type as ApiKeysPurpose);
  };

  const addApiKey = async (): Promise<boolean> => {
    const purpose = apiKeysPurpose;
    let promise = onSave(name, purpose);
    // we need to reset checkbox value to initial state here
    return promise.then(x => { reset(); return x });
  };

  const reset = (): void => {
    setApiKeysPurposes(ApiKeysPurpose.Delivery);
  }

  const apiKeysPurposes: Array<LabelValuePair<string>> = [
    {
      value: ApiKeysPurpose.Delivery.toString(),
      label: messages.delivery,
    },
    {
      value: ApiKeysPurpose.Preview.toString(),
      label: messages.preview,
    },
  ];

  return (
    <OpenModalOperation
      variant="contained"
      displayMode="iconLabel"
      label={messages.apiKey}
      icon="m-icon-plus"
      color="primary"
      id="add"
      modalTitle={messages.apiKey}
      initializeModalAsync={initializeModal}
      modalType="SmallModal"
      fullWidth={true}
      lowerCase={true}
      modalActions={[
        <ModalOperation
          isValid={addApiKey}
          closeModal={reset}
          displayMode="label"
          label={messages.create}
          key="create_parent"
          variant="contained"
          color="primary"
          disabled={name === ''}
          id="create"
        />,
        <ModalOperation
          onClick={reset}
          displayMode="label"
          label={messages.cancel}
          key="cancel"
          variant="text"
          color="primary"
          id="cancel"
        />,
      ]}
    >
      <Container maxWidth="md">
        <Label
          label={messages.name}
          isRequired
          labelPosition="left"
        >
          <TextEditor
            onChange={onNameChanged}
            errorMessage={messages.required}
            isRequired
            value={name}
            fullWidth
            id="name"
            regex={regex}
            labelPosition="left"
          />
        </Label>
        <Label label={messages.purpose}
          labelPosition="left" >
          <RadioButtonEditor
            items={apiKeysPurposes}
            id="apiKeyPurposeSelector"
            onChange={newValue => onPurposeChanged(newValue as string)}
            value={apiKeysPurpose}
            labelPosition="left"
          />
        </Label>
      </Container>
    </OpenModalOperation>
  )
};

export default AddApiKeyOperationsComponent;