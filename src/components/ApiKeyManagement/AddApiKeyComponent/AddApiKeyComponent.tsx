import React, { ReactElement, useState } from "react";
import AddApiKeyOperationsComponent from "Components/ApiKeyManagement/AddApiKeyComponent/AddApiKeyOperationsComponent";
import AddApiKeyWarningComponent from "Components/ApiKeyManagement/AddApiKeyComponent/AddApiKeyWarningComponent";
import { createApiKeyAsync } from 'API';

interface AddApiKeyComponentProps {
  messages: Record<string, string>,
  onAddApiKey: () => void,
  allowedManageKeys: boolean,
}

const AddApiKeyComponent: React.FunctionComponent<AddApiKeyComponentProps> = ({
  messages,
  allowedManageKeys,
  onAddApiKey
}): ReactElement => {

  const [keyCode, setKeyCode] = useState("");
  const [showModal, setShowModal] = useState(false);
  const addApiKey = async (apiKeyName: string, apiKeysPurpose: string): Promise<boolean> => {
    const response = await createApiKeyAsync(apiKeyName, apiKeysPurpose);
    if (response.isSuccessStatusCode) {
      const responseContent = response.content as string;
      setKeyCode(responseContent);
      setShowModal(true);
      onAddApiKey()
      return true;
    }
    else {
      return false;
    }
  };

  const onCloseModal = (): void => {
    setShowModal(false);
  };

  const getContent = () => {
    if (!allowedManageKeys) {
      return (<div className="margin-left-default">
        <span className="alert-warning-inverted">{messages.apiKeyFunctionalityDisabled}</span>
      </div>)
    }

    return <AddApiKeyOperationsComponent messages={messages} onSave={addApiKey} />;
  }

  return (
    <>
      {
        getContent()
      }
      <AddApiKeyWarningComponent messages={messages} keyCode={keyCode} showModal={showModal} onCloseModal={onCloseModal} />
    </>
  );
};

export default AddApiKeyComponent;
