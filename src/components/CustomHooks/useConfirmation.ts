import useMessagesContext from "Contexts/MessagesContext";
import { ConfirmationConfig } from "Interfaces/CommonComponents";
import { createConfirmation } from "react-confirm";

import ConfirmationDialog from "components/Common/Confirmation/ConfirmationDialog";

/* example of how to use

import confirm from "components/Common/Confirmation/ConfirmationComponent";

const handleOnClick = () => {
        confirm("Some text to show", "Some title", "ok", "cancel").then(
            () => {
                console.log("proceed!");
            },
            () => {
                console.log("cancel!");
            }
        );
    };

*/

const defaultMessages = {
    ok: "Ok",
    cancel: "Cancel",
    confirm: "Confirm",
};

const useConfirmation = (): Array<Function> => {
    const createConfirm = createConfirmation(ConfirmationDialog);
    const messages = useMessagesContext(defaultMessages);

    const confirm = (text: string, title?: string, ok?: string, cancel?: string): Promise<string> => {
        // if a ok, cancel or title is not provided take the default from messages
        const config: ConfirmationConfig = {
            okLabel: ok || messages.ok,
            cancelLabel: cancel || messages.cancel,
            confirmTitle: title || messages.confirm,
            confirmMessage: text,
        };

        return createConfirm({ config });
    };

    return [confirm];
};

export default useConfirmation;
