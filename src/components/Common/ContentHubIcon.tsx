import classNames from "classnames";
import React, { FC } from "react";

interface ContentHubIconProps extends React.HTMLProps<HTMLElement> {
    icon: string;
    classes?: Array<string | null>;
    tagName?: "i" | "em";
    label?: string;
}

const ContentHubIcon: FC<ContentHubIconProps> = ({ icon: icon_, classes, tagName = "i", label, ...props }) => {
    const icon = icon_?.startsWith("m-icon-") ? icon_ : `m-icon-${icon_}`;

    if (tagName === "i") {
        return <i className={classNames("m-icon", classes, icon)} {...props} aria-label={label} />;
    }

    return <em className={classNames("m-icon", classes, icon)} {...props} aria-label={label} />;
};

export default ContentHubIcon;
