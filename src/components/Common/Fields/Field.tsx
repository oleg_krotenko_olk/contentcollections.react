import React, { FunctionComponent, ReactElement } from "react";

// props
export interface FieldProps {
    /**
     * The field text or content
     */
    label: string | ReactElement;
}

// component
const Field: FunctionComponent<FieldProps> = ({ label }): ReactElement => {
    return (
        <>
            <span>{label}</span>
        </>
    );
};

export default Field;
