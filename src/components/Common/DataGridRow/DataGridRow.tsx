import React, { cloneElement, FunctionComponent, ReactElement } from "react";
import { isFunction } from "Utils";

import { makeStyles, TableCell, TableRow, useTheme } from "@material-ui/core";

import { FieldProps } from "Components/Common/Fields/Field";
import { OperationProps } from "Components/Common/Operations/Operation";

// styles
const useStyles = makeStyles(theme => ({
    root: {
        width: "100%",
        marginTop: theme.spacing(3),
        overflowX: "auto",
    },
    tableIndicator: {
        "marginTop": "8px",
        "marginRight": theme.spacing(1),
        "display": "inline-flex",
        "&:last-child": {
            marginRight: 0,
        },
    },
    tableOperation: { marginTop: "2.5px" },
    clickableRow: {
        cursor: "pointer",
    },
    breakWord: {
        wordBreak: "break-word",
    },
    operationsCell: {
        "verticalAlign": "top",
        "padding": `0 ${theme.spacing(2)}px 0 0 `,
        "whiteSpace": "nowrap",

        /* this is needed because the child can be a button or a tooltip */
        "& > *": {
            "marginRight": theme.spacing(1),

            "&:last-child": {
                marginRight: "-14px",
            },
        },
        "width": "1%",
    },
    indicatorsCell: {
        verticalAlign: "top",
        padding: `0 ${theme.spacing(2)}px 0 0 `,
        whiteSpace: "nowrap",
    },
}));

export interface DataGridRowProps {
    /**
     * Id for the row
     */
    id: string;
    /**
     * Array of operations
     */
    operations?: Array<ReactElement<OperationProps>>;

    /**
     * Array of fields
     */
    fields?: Array<ReactElement<FieldProps>>;
    /**
     * The (main) click action when clicking the entire row
     */
    onClick?: () => void; // main click action
    /**
     * Indicate if there should be a hover effect on the row
     */
    hover?: boolean;
    /**
     * Indicate if the indicators should be shown
     */
    shouldShowIndicators?: boolean;
    /**
     * Indicate if the operations should show
     */
    shouldShowOperations?: boolean;
    /**
     * Indicated if the fields should be shown
     */
    shouldShowFields?: boolean;
}

// component
const DataGridRow: FunctionComponent<DataGridRowProps> = ({
    id,
    operations,
    fields,
    onClick: onClick_,
    hover = true,
    shouldShowOperations = true,
    shouldShowFields = true,
}): ReactElement => {
    const theme = useTheme();
    const classes = useStyles(theme);

    // add extra class to the operations
    const getOperations = (): Array<ReactElement<OperationProps>> => {
        if (!operations) {
            return [];
        }

        return operations.map(operation => {
            return cloneElement(
                operation,
                {
                    className: classes.tableOperation,
                },
                null
            );
        });
    };

    const onClick = (): void => {
        if (isFunction(onClick_)) {
            onClick_();
        }
    };

    return (
        <TableRow
            hover={hover}
            className={onClick_ ? classes.clickableRow : undefined}
            data-testid={id}
            onClick={onClick}
        >
            {shouldShowFields
                ? fields?.map(field => {
                      return (
                          <TableCell align="left" className={classes.breakWord} key={field.key}>
                              {field}
                          </TableCell>
                      );
                  })
                : null}
            {shouldShowOperations ? (
                <TableCell align="right" className={classes.operationsCell}>
                    {getOperations()}
                </TableCell>
            ) : null}
        </TableRow>
    );
};

export default DataGridRow;
