import useMessagesContext from "Contexts/MessagesContext";
import React, { FunctionComponent, ReactElement } from "react";

import { Box, makeStyles, useTheme } from "@material-ui/core";
import { textColorLight, textColorLightOnDark } from "ThemeVariables";

const useStyles = makeStyles(theme => ({
    noResults: {
        textAlign: "center",
        padding: theme.spacing(4),
        color: theme.palette.type === "dark" ? textColorLightOnDark : textColorLight,
    },
}));

const defaultMessages = {
    noResults: "No results",
};

interface NoResultsTextProps {
    /**
     * The message to be shown
     */
    message?: string;
}

const NoResultsText: FunctionComponent<NoResultsTextProps> = ({ message }): ReactElement => {
    const theme = useTheme();
    const classes = useStyles(theme);
    const messages = useMessagesContext(defaultMessages);

    return <Box className={classes.noResults}>{message || messages.noResults}</Box>;
};

export default NoResultsText;
