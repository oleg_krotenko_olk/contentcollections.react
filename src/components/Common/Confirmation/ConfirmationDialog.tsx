import Theme from "ContentHubThemes";
import { ConfirmationConfig } from "Interfaces/CommonComponents";
import React, { FunctionComponent, ReactElement } from "react";
import { confirmable } from "react-confirm";

import { Button, Dialog, DialogActions, DialogContent, DialogTitle, ThemeProvider } from "@material-ui/core";
import Operation from "../Operations/Operation";

/**
 * Interface for the {@link ConfirmationDialog}
 */
interface ConfirmationDialogProps {
    /**
     * Indicates if the dialog is shown or not.
     */
    show: boolean;

    /**
     * Function to close the dialog which resolves the confirmation promise.
     * @remarks
     * See confirmable
     * @param value - Value to resolve the promise with
     */
    proceed: (value?: unknown) => void;

    /**
     * Function to close the dialog which rejects the confirmation promise.
     * @remarks
     * See confirmable
     * @param value - Value to reject the promise with
     */
    cancel: (value?: unknown) => void;

    /**
     * Configuration for the dialog.
     */
    config: ConfirmationConfig;
}

/**
 * ConfirmationDialog component.
 * @param props - Props for the component
 */
const ConfirmationDialog: FunctionComponent<ConfirmationDialogProps> = ({
    show,
    proceed,
    cancel,
    config,
}): ReactElement => {
    return (
        <ThemeProvider theme={Theme}>
            <Dialog
                disableBackdropClick
                disableEscapeKeyDown
                maxWidth="xs"
                fullWidth
                aria-labelledby="confirmation-dialog-title"
                open={show}
                onClose={cancel}
            >
                <DialogTitle id="confirmation-dialog-title">{config.confirmTitle}</DialogTitle>
                <DialogContent>{config.confirmMessage}</DialogContent>
                <DialogActions>
                    <Operation
                        label={config.okLabel}
                        id="confirm-ok"
                        variant="contained"
                        color="primary"
                        onClick={() => proceed(true)}
                        data-testid="confirmation-ok"
                    />
                    <Operation
                        label={config.cancelLabel}
                        id="confirm-ok"
                        color="primary"
                        onClick={cancel}
                        data-testid="confirmation-cancel"
                    />
                </DialogActions>
            </Dialog>
        </ThemeProvider>
    );
};

export default confirmable(ConfirmationDialog);
