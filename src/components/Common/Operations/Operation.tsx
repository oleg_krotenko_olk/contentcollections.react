import classNames from "classnames";
import Color from "color";
import React, { ReactElement, useEffect } from "react";

import { Nullable } from "@sitecore/sc-contenthub-webclient-sdk/dist/base-types";

import {
    Badge,
    Button,
    CircularProgress,
    Link,
    ListItemIcon,
    makeStyles,
    MenuItem,
    PropTypes,
    Tooltip,
    TooltipProps,
    Typography,
    useTheme,
} from "@material-ui/core";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import {
    btnPrimaryActiveBg,
    btnPrimaryBg,
    btnPrimaryColor,
    btnPrimaryFocusBg,
    btnPrimaryHoverBg,
    fontWeightBold,
    grayColorTransparent,
    iconSizeSmall,
    mainColor,
    themePrimary,
    whiteColorTransparent,
} from "ThemeVariables";

import { isFunction } from "Utils";

import ContentHubIcon from "Components/Common/ContentHubIcon";

const primaryColor: Color = Color(mainColor);

const documentDirection = document.dir;

const arrowDropDownIconStyle = {
    marginRight: documentDirection === "ltr" ? "-4px" : 0,
    marginLeft: documentDirection === "ltr" ? 0 : "-4px",
};

const useStyles = makeStyles(theme => ({
    button: {
        minWidth: 0,
    },
    iconLabel: {
        "flip": false,
        "marginLeft": -theme.spacing(0.5),
        "marginRight": -theme.spacing(0.5),
        "& + span": {
            marginLeft: theme.spacing(1),
        },
    },
    labelLeft: {
        marginRight: theme.spacing(1),
    },
    labelRight: {
        marginLeft: theme.spacing(1),
    },
    icon: {
        marginLeft: -theme.spacing(0.5),
        marginRight: -theme.spacing(0.5),
    },

    smallIcon: {
        fontSize: iconSizeSmall,
        marginLeft: 0,
        marginRight: 0,
    },
    dropdownLink: {
        textDecoration: "none !important",
        color: "inherit !important",
    },

    // light button variant
    lightButton: {
        "backgroundColor": "transparent !important",
        "color": `${grayColorTransparent[400]}  !important`,
        "&:hover": {
            backgroundColor: "transparent !important",
            color: `${grayColorTransparent[700]}  !important`,
        },
        "&:focus": {
            backgroundColor: "transparent !important",
            color: `${grayColorTransparent[800]}  !important`,
        },
        "&:active": {
            backgroundColor: "transparent !important",
            color: `${grayColorTransparent[900]}  !important`,
        },
        "&.disabled, &[disabled]": {
            backgroundColor: "transparent !important",
        },
    },

    // button overrides for the dark mode
    text: {
        "color": "white !important",
        "&:hover": {
            backgroundColor: `${whiteColorTransparent[200]}  !important`,
            color: "white !important",
        },
        "&:focus": {
            backgroundColor: `${whiteColorTransparent[300]}  !important`,
            color: "white !important",
        },
        "&:active": {
            backgroundColor: `${whiteColorTransparent[400]}  !important`,
            color: "white !important",
        },
        "&.disabled, &[disabled]": {
            backgroundColor: "transparent !important",
            color: `${whiteColorTransparent[500]}  !important`,
        },
    },
    outlined: {
        "color": "white !important",
        "borderColor": `${whiteColorTransparent[400]}  !important`,
        "backgroundColor": "transparent !important",
        "&:hover": {
            backgroundColor: `${whiteColorTransparent[200]}  !important`,
            color: "white !important",
        },
        "&:focus": {
            backgroundColor: `${whiteColorTransparent[300]}  !important`,
            color: "white !important",
        },
        "&:active": {
            backgroundColor: `${whiteColorTransparent[400]}  !important`,
            color: "white !important",
        },
        "&.disabled, &[disabled]": {
            backgroundColor: "transparent !important",
            color: `${whiteColorTransparent[500]}  !important`,
        },
    },
    contained: {
        "color": `${theme.palette.primary.main}  !important`,
        "backgroundColor": "white !important",
        "&:hover": {
            backgroundColor: `${primaryColor.mix(Color("white"), 9.3)}  !important`,
            color: `${theme.palette.primary.main}  !important`,
        },
        "&:focus": {
            backgroundColor: `${primaryColor.mix(Color("white"), 8.8)}  !important`,
            color: `${theme.palette.primary.main}  !important`,
        },
        "&:active": {
            backgroundColor: `${primaryColor.mix(Color("white"), 7.4)}  !important`,
            color: `${theme.palette.primary.main}  !important`,
        },
        "&.disabled, &[disabled]": {
            backgroundColor: `${whiteColorTransparent[100]}  !important`,
            color: `${whiteColorTransparent[500]}  !important`,
        },
    },
    containedNonDark: {
        "color": `${btnPrimaryColor}  !important`,
        "backgroundColor": `${btnPrimaryBg}  !important`,
        "&:hover": {
            backgroundColor: `${btnPrimaryHoverBg}  !important`,
            color: `${btnPrimaryColor}  !important`,
        },
        "&:focus": {
            backgroundColor: `${btnPrimaryFocusBg}  !important`,
            color: `${btnPrimaryColor}  !important`,
        },
        "&:active": {
            backgroundColor: `${btnPrimaryActiveBg}  !important`,
            color: `${btnPrimaryColor}  !important`,
        },
        "&.disabled, &[disabled]": {
            backgroundColor: `rgba(0, 0, 0, 0.12) !important`,
            color: `rgba(0, 0, 0, 0.26) !important`,
        },
    },
    lightButtonOnDark: {
        "backgroundColor": "transparent !important",
        "color": `${whiteColorTransparent[700]}  !important`,
        "&:hover": {
            backgroundColor: "transparent !important",
            color: `${whiteColorTransparent[800]}  !important`,
        },
        "&:focus": {
            backgroundColor: "transparent !important",
            color: `${whiteColorTransparent[900]}  !important`,
        },
        "&:active": {
            backgroundColor: "transparent !important",
            color: "white  !important",
        },
        "&.disabled, &[disabled]": {
            backgroundColor: "transparent !important",
            color: `${whiteColorTransparent[500]}  !important`,
        },
    },
    lowerCase: {
        textTransform: "none !important" as "none",
        color: theme.palette.text.primary,
    },
    lowerCaseDark: {
        textTransform: "none !important" as "none",
        color: whiteColorTransparent["800"],
    },
    loader: {
        position: "absolute",
        left: "50%",
        marginLeft: "-7px",
        opacity: ".5",
    },
    showLoader: {
        "& > .MuiButton-label >:not(.MuiCircularProgress-root)": {
            visibility: "hidden",
        },
    },
    activeMenuItem: {
        color: mainColor,
        fontWeight: fontWeightBold,
    },
    underLineOnHover: {
        "color": mainColor,
        "textTransform": "none !important" as "none",
        "&:hover": {
            textDecoration: "underline",
        },
    },
    normalFontWeightLabel: {
        fontWeight: 400,
        textTransform: "none !important" as "none",
    },
    dropdownIconOnly: {
        minWidth: 0,
    },
    operationCount: {
        color: mainColor,
        backgroundColor: themePrimary[50],
        borderRadius: 40,
        padding: "3px 5px",
        fontSize: 10,
        fontWeight: fontWeightBold,
        lineHeight: 11,
        zIndex: 10,
        minWidth: 16,
        textAlign: "center",
        fontStyle: "normal",
        height: 17,
    },
    operationCountDropdownWrapper: {
        width: "100%",
    },
    operationCountDropdownItem: {
        color: grayColorTransparent[700],
        backgroundColor: grayColorTransparent[200],
        borderRadius: 40,
        padding: "1px 8px",
        fontSize: 12,
        fontWeight: fontWeightBold,
        lineHeight: 11,
        zIndex: 10,
        minWidth: 28,
        textAlign: "center",
        fontStyle: "normal",
        height: 21,
        top: 10,
        right: -25,
    },
}));

export type ButtonSize = React.ComponentProps<typeof Button>["size"];
export type TooltipPosition = TooltipProps["placement"];

export type DisplayModeWithIcon = "icon" | "iconLabel";
export type DisplayModeWithoutIcon = "label";
export type DisplayMode = DisplayModeWithIcon | DisplayModeWithoutIcon;
export type DisplayType = "default" | "primary" | "secondary" | "link" | "none";

export interface OperationDisplayProps {
    /**
     * The display mode of the operation.
     */
    displayMode?: Nullable<DisplayMode>;

    /**
     * The text to show on the button
     */
    label: string;

    /**
     * The id of the component
     */
    id: string; // for jest test to be able to add an id and find the button

    /**
     * The color of the button (primary, secondary, default)
     */
    color?: Nullable<PropTypes.Color>;

    /**
     * The class of the icon that needs to show e.g. m-icon-trash
     */
    icon?: Nullable<string>;

    /**
     * The text to show on hover of operations.
     */
    title?: string;

    /**
     * The look of the button (text = normal, outlined = with a border, contained = with a background color based on the color property)
     */
    variant?: "text" | "outlined" | "contained";

    /**
     * A css class to overrule some styling
     */
    className?: string;

    /**
     * Indicates if  the button is disabled or not
     */
    disabled?: boolean;

    /**
     * When enabled it shows the button in a lighter modus
     */
    lightButton?: boolean; // to mimic the previous btn-light

    /**
     *
     */
    isVisible?: boolean;

    /**
     * Select a size for the button (default is medium)
     */
    size?: ButtonSize;

    /**
     * An id of another element that is described by this operation
     */
    ariaDescribedBy?: string;

    /**
     * Indicate if the operation should show a dropdown caret
     */
    showCaret?: boolean;

    /**
     * Indicate if the operation should shown as a menu item in a dropdown
     */
    showAsMenuItem?: boolean;

    /**
     * Indicate if the operation controls a popup
     */
    ariaHasPopup?: boolean | "false" | "true" | "menu" | "listbox" | "tree" | "grid" | "dialog";

    /**
     * An id of another element that is controlled by this operation
     */
    ariaControls?: string;

    /**
     * The position of the tooltip
     */
    tooltipPosition?: TooltipPosition;

    /**
     * Indicates that the text displayed on the button needs to be lowercase
     */
    lowerCase?: boolean;

    /**
     * Indicates that the click event may propagate (false by default)
     */
    allowPropagation?: boolean;

    /**
     * Indicates that a loading spinner should be shown on the button
     */
    showLoader?: boolean;

    /**
     * used together with showAsMenuItem to indicate a menu item as the active item
     */
    active?: boolean;

    /**
     * A function to be used to close the parent dropdown when clicking the operation in a operationDropdown
     */
    handleMenuClose?: () => void;

    /**
     * A boolean to indicate if the operation should be underlined on hover
     */
    underlineOnHover?: boolean;

    /**
     * This function is used in the operations dropdown to let it know if the operation is not visible (if the dropdown only renders invisible operations it should not render)
     */
    isVisibleCheck?: (name: string, visible: boolean) => void;

    /**
     * The target if the operation renders as a link
     */
    target?: "_blank" | "_self";

    /**
     * Disable the ripple effect on the operation
     */
    disableRipple?: boolean;

    /**
     * The forwarded ref
     */
    ref?: React.Ref<any>;
    /**
     * Icon position on the button
     */
    iconPosition?: "left" | "right";
    /**
     * The display type affects the color and variant of the button
     */
    displayType?: DisplayType;
    /**
     * Force title to show also on label buttons
     */
    showTitle?: boolean;
    /**
     * A css class to overrule some tooltip styling
     */
    tooltipClassName?: string;

    /**
     * An optional type
     */
    buttonType?: "submit" | "reset" | "button";

    /**
     * An optional number to show on the operation e.g. number of annotations or public links
     */
    operationCount?: number;
}

export interface OperationProps extends OperationDisplayProps {
    /**
     * The action that needs to happen when clicking on the operation
     */
    onClick?: (event?: any) => void;

    /**
     * Renders the button as a link with the href
     */
    href?: string | null | undefined;
}

export interface ExtendedOperationProps extends OperationProps {
    render?: (props: React.PropsWithRef<OperationProps>) => JSX.Element;
}

const Operation = React.forwardRef(function Operation(
    {
        displayMode = "label",
        label,
        id,
        icon = "m-icon-bicycle",
        title: title_,
        color = "default",
        variant = "text",
        className,
        disabled = false,
        lightButton,
        isVisible = true,
        href = null,
        size = "medium",
        ariaDescribedBy,
        showCaret,
        showAsMenuItem,
        ariaHasPopup,
        ariaControls,
        tooltipPosition = "top",
        lowerCase,
        allowPropagation = false,
        showLoader = false,
        active,
        handleMenuClose,
        underlineOnHover,
        isVisibleCheck,
        onClick: onClick_,
        target,
        disableRipple = false,
        render,
        iconPosition = "left",
        displayType = "none",
        showTitle = false,
        tooltipClassName,
        buttonType = "button",
        operationCount = 0,
    }: ExtendedOperationProps,
    ref?: React.Ref<any>
) {
    const theme = useTheme();
    const darkMode = theme.palette.type === "dark";
    const classes = useStyles(theme);
    const title = title_ || label;

    useEffect(() => {
        if (isFunction(isVisibleCheck) && id !== undefined) {
            isVisibleCheck(id.toString(), isVisible);
        }
    }, [isVisible, isVisibleCheck, id]);

    const onClick = (event: React.MouseEvent<HTMLAnchorElement | HTMLSpanElement, MouseEvent>): void => {
        if (!allowPropagation && !showAsMenuItem) {
            event.stopPropagation();
        }

        if (isFunction(onClick_)) {
            onClick_(event);
        }

        if (isFunction(handleMenuClose)) {
            handleMenuClose();
        }
    };

    let variantToDisplay = variant;
    let colorToDisplay = color;

    if (displayType === "primary") {
        variantToDisplay = "contained";
        colorToDisplay = "primary";
    }

    if (displayType === "secondary") {
        variantToDisplay = "outlined";
        colorToDisplay = "primary";
    }

    if (displayType === "link") {
        colorToDisplay = "primary";
    }

    // Inner button markup
    const innerButtonMarkup = (
        <>
            {showLoader && <CircularProgress size={14} className={classes.loader} />}
            {(displayMode === "iconLabel" && iconPosition === "left") || displayMode === "icon" ? (
                <ContentHubIcon
                    icon={icon!}
                    classes={[
                        (displayMode as string) === "iconLabel" ? classes.iconLabel : null,
                        displayMode === "icon" ? classes.icon : null,
                        size === "small" ? classes.smallIcon : null,
                    ]}
                    label={label}
                />
            ) : null}
            {displayMode === "iconLabel" || displayMode === "label" ? (
                <span
                    className={classNames(
                        displayMode === "iconLabel" && iconPosition === "right" ? classes.labelLeft : undefined,
                        displayMode === "iconLabel" && iconPosition === "left" ? classes.labelRight : undefined
                    )}
                >
                    {label}
                </span>
            ) : null}
            {displayMode === "iconLabel" && iconPosition === "right" ? (
                <ContentHubIcon
                    icon={icon!}
                    classes={[classes.iconLabel, size === "small" ? classes.smallIcon : null]}
                    label={label}
                />
            ) : null}
            {showCaret ? <ArrowDropDownIcon style={arrowDropDownIconStyle} /> : null}
        </>
    );

    // Inner menuItemButton markup
    const innerMenuItemButtonMarkup = (
        <>
            {showLoader && <CircularProgress size={14} className={classes.loader} />}
            {displayMode === "iconLabel" || displayMode === "icon" ? (
                <ListItemIcon className={displayMode === "icon" ? classes.dropdownIconOnly : undefined}>
                    <ContentHubIcon
                        icon={icon!}
                        classes={[displayMode === "iconLabel" ? classes.iconLabel : null]}
                        label={label}
                    />
                </ListItemIcon>
            ) : null}
            {displayMode === "iconLabel" || displayMode === "label" ? (
                <Typography variant="inherit">{label}</Typography>
            ) : null}
        </>
    );

    const getButton = (): ReactElement => {
        // This is used for the main operation on the search component
        return isFunction(render) ? (
            render({ onClick, disabled, id, label, href, title, target, ref })
        ) : (
            <Button
                onClick={onClick}
                variant={variantToDisplay}
                disabled={disabled}
                color={
                    variantToDisplay === "outlined" && theme.palette.type !== "dark"
                        ? "primary"
                        : colorToDisplay ?? undefined
                }
                href={href as string}
                target={target}
                size={size}
                aria-describedby={ariaDescribedBy}
                className={classNames(
                    classes.button,
                    className,
                    darkMode ? classes[variant] : null,
                    lightButton && darkMode ? classes.lightButtonOnDark : null,
                    lightButton && !darkMode ? classes.lightButton : null,
                    lowerCase && darkMode ? classes.lowerCaseDark : null,
                    lowerCase && !darkMode ? classes.lowerCase : null,
                    showLoader ? classes.showLoader : null,
                    underlineOnHover ? classes.underLineOnHover : null,
                    !darkMode && variant === "contained" && color === "primary" ? classes.containedNonDark : null
                )}
                disableRipple={lightButton || disableRipple}
                data-testid={id}
                aria-haspopup={ariaHasPopup}
                aria-controls={ariaControls}
                aria-label={label}
                classes={{
                    label: classNames(
                        lowerCase ? classes.normalFontWeightLabel : null,
                        underlineOnHover ? classes.underLineOnHover : null
                    ),
                }}
                ref={ref}
                disableElevation
                type={buttonType}
            >
                {operationCount > 0 ? (
                    <Badge
                        badgeContent={operationCount}
                        invisible={operationCount === 0}
                        color="primary"
                        classes={{ colorPrimary: classes.operationCount }}
                    >
                        {innerButtonMarkup}
                    </Badge>
                ) : (
                    innerButtonMarkup
                )}
            </Button>
        );
    };

    const getMenuItemButton = (): ReactElement => {
        return !href ? (
            <MenuItem
                onClick={onClick}
                disabled={disabled}
                className={active ? classes.activeMenuItem : undefined}
                component="span"
                data-testid={id}
            >
                {operationCount > 0 ? (
                    <Badge
                        badgeContent={operationCount}
                        invisible={operationCount === 0}
                        color="primary"
                        classes={{ colorPrimary: classes.operationCountDropdownItem }}
                        className={classes.operationCountDropdownWrapper}
                    >
                        {innerMenuItemButtonMarkup}
                    </Badge>
                ) : (
                    innerMenuItemButtonMarkup
                )}
            </MenuItem>
        ) : (
            <Link href={href} className={classNames(classes.dropdownLink)} target={target} data-testid={id}>
                <MenuItem className={active ? classes.activeMenuItem : undefined} component="span">
                    {operationCount > 0 ? (
                        <Badge
                            badgeContent={operationCount}
                            invisible={operationCount === 0}
                            color="primary"
                            classes={{ colorPrimary: classes.operationCountDropdownItem }}
                            className={classes.operationCountDropdownWrapper}
                        >
                            {innerMenuItemButtonMarkup}
                        </Badge>
                    ) : (
                        innerMenuItemButtonMarkup
                    )}
                </MenuItem>
            </Link>
        );
    };

    if (!isVisible) {
        return null;
    }

    if (showAsMenuItem) {
        return getMenuItemButton();
    }

    if (displayMode === "icon" || showTitle) {
        return (
            <>
                <Tooltip title={title} placement={tooltipPosition} className={tooltipClassName}>
                    <span>{getButton()}</span>
                </Tooltip>
            </>
        );
    }

    return getButton();
});

// this form of export is needed to make the component name display correctly in the styleguide
export default React.memo(Operation as React.FC<ExtendedOperationProps>);
