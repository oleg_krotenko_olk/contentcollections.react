import React, { ReactElement, useCallback, useState } from "react";

import { RootRef } from "@material-ui/core";

import { OptionsContextProvider } from "Contexts/OptionsContextProvider";

import Modal, { ModalProps } from "Components/Common/Modal/Modal";
import { ModalOperationProps } from "Components/Common/Operations/OpenModal/ModalOperation";
import Operation, { OperationDisplayProps } from "Components/Common/Operations/Operation";
import { isFunction } from "Utils";

// props
interface OwnProps {
    id: string;
    modalActions?: Array<ReactElement<ModalOperationProps>>;
    initializeModalAsync?: () => Promise<void>;
    children?: React.ReactNode;
}

export type OpenModalOperationProps = OwnProps & OperationDisplayProps & ModalProps;

// component
const OpenModalOperation = React.forwardRef(function OpenModalOperation(
    { modalActions = [], handleMenuClose, initializeModalAsync, children, ...props }: OpenModalOperationProps,
    ref?: React.Ref<never>
) {
    // boolean to show/hide the modal
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [openModalButton, setOpenModalButton] = useState<HTMLElement>();
    const openModalButtonRef = useCallback((node: HTMLElement) => {
        if (node !== null) {
            setOpenModalButton(node);
        }
    }, []);

    const onModalClose = (): void => {
        setIsModalVisible(false);

        if (isFunction(handleMenuClose)) {
            handleMenuClose();
        }

        // put the focus back on the operation after closing the modal so that we can open it again with a simple enter press
        if (openModalButton?.tagName === "BUTTON") {
            openModalButton?.focus();
        } else {
            // when there is a tooltip around the icon button we need to find the button inside it
            const button = openModalButton?.querySelector("button");

            button?.focus();
        }
    };

    return (
        <>
            <RootRef rootRef={openModalButtonRef}>
                <Operation
                    onClick={async (event: React.MouseEvent<HTMLAnchorElement | HTMLButtonElement, MouseEvent>) => {
                        event.preventDefault();

                        if (isFunction(initializeModalAsync)) {
                            await initializeModalAsync();
                        }

                        setIsModalVisible(true);
                    }}
                    {...(props as OperationDisplayProps)}
                    handleMenuClose={() => { }}
                    disableRipple
                    ref={ref}
                />
            </RootRef>
            {isModalVisible && (
                <Modal
                    modalVisible={isModalVisible}
                    modalActions={modalActions.map(modalOperation => {
                        return React.cloneElement(
                            modalOperation,
                            {
                                closeModal: onModalClose,
                            },
                            null
                        );
                    })}
                    onModalClose={onModalClose}
                    {...props}
                >
                    <OptionsContextProvider isInModal>{children}</OptionsContextProvider>
                </Modal>
            )}
        </>
    );
});

export default OpenModalOperation;
