import React, { ReactElement } from "react";
import { isFunction } from "Utils";

import Operation, { OperationProps } from "Components/Common/Operations/Operation";

interface OwnProps {
    isValid?: () => Promise<boolean>;
    closeModal?: () => void;
}

export type ModalOperationProps = OwnProps & OperationProps;

// component
const ModalOperation: React.FunctionComponent<ModalOperationProps> = ({
    closeModal,
    isValid,
    onClick,
    ...props
}): ReactElement => {
    return (
        <Operation
            onClick={async () => {
                // if a validation function is defined, only close the modal if it returns true
                if (!isValid || (await isValid())) {
                    if (isFunction(onClick)) {
                        onClick();
                    }

                    if (closeModal) {
                        closeModal();
                    }
                }
            }}
            {...props}
        />
    );
};

export default ModalOperation;
