import classNames from "classnames";
import { LabelValuePair } from "Interfaces/ValuePairs";
import React, { FunctionComponent, ReactElement, useEffect, useState } from "react";
import { isFunction } from "Utils";

// combine class names
import { FormControlLabel, makeStyles, Radio, RadioGroup, Typography, useTheme } from "@material-ui/core";

import { BaseEditorProps } from "Components/Common/Editors/BaseEditorProps";
import EditorWrapper from "Components/Common/Editors/EditorWrapper";

// styles
const useStyles = makeStyles(theme => ({
    text: {
        marginTop: theme.spacing(1.2),
    },

    whiteText: {
        color: "white !important",
    },
    darkModeRadioButton: {
        color: "white !important",
    },
}));

// props
interface SpecificProps {
    /**
     * The array of items to choose from
     */
    items: Array<LabelValuePair<string>> | Array<LabelValuePair<number>>;

    /**
     * Indicates if the radio buttons should be shown on top instead of next to each other
     */
    verticalMode?: boolean;
}

export type RadioButtonEditorProps = BaseEditorProps<string | number> & SpecificProps;

// component
const RadioButtonEditor: FunctionComponent<RadioButtonEditorProps> = ({
    value: value_,
    isRequired,
    onChange: onChange_,
    resetIsDirty,
    disabled,
    id,
    className,
    accessibilityId,
    isReadonly,
    labelPosition,
    items,
    verticalMode,
    ...props
}): ReactElement => {
    const theme = useTheme();
    const darkMode = theme.palette.type === "dark";
    const classes = useStyles(theme);
    const [value, setValue] = useState(value_ || items[0].value);

    // default read modus typography variant
    const typographyVariant = "body1";

    /**
     * Select a value in the radio group
     * @param event -  event
     */
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        const val = (event.target as HTMLInputElement).value;

        setValue(val);

        if (isFunction(onChange_)) {
            onChange_(val);
        }
    };

    useEffect(() => {
        setValue(value_);
    }, [value_]);

    const getEditor = (): ReactElement => {
        if (isReadonly) {
            return (
                <Typography
                    variant={typographyVariant || "body1"}
                    className={classNames(
                        labelPosition === "left" ? classes.text : null,
                        darkMode ? classes.whiteText : null
                    )}
                    data-testid={id}
                >
                    {value}
                </Typography>
            );
        }

        return (
            <RadioGroup
                aria-label="radioButtonEditor"
                name="radioButtonEditor"
                value={value}
                onChange={handleChange}
                data-testid={id}
                id={id}
                row={!verticalMode}
            >
                {(items as Array<LabelValuePair<string>>).map((item: LabelValuePair<string>) => {
                    return (
                        <FormControlLabel
                            value={item.value}
                            control={
                                <Radio color={darkMode ? "default" : "primary"} className={classNames(className)} />
                            }
                            label={item.label}
                            labelPlacement="end"
                            key={item.value}
                            className={classNames(className, darkMode ? classes.whiteText : null)}
                        />
                    );
                })}
            </RadioGroup>
        );
    };

    return (
        <EditorWrapper isReadonly={isReadonly} labelPosition={labelPosition} {...props}>
            {getEditor()}
        </EditorWrapper>
    );
};

export default RadioButtonEditor;
