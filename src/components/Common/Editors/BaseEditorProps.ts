export interface BaseEditorProps<T> {
    /**
     * The id of the component
     */
    id: string;

    /**
     * The value for the editor
     */
    value: T;

    /**
     * A random id that will be used for accessibility
     * (i.e. "for" attribute on label should have the same id as the input)
     */
    accessibilityId?: string;

    /**
     * Indicates if the editor is disabled
     */
    disabled?: boolean;

    /**
     * Indicates if it is required to give a value
     */
    isRequired?: boolean;

    /**
     * Custom class name to overrule some styling
     */
    className?: string;

    /**
     * Indicates if the dirty value needs to be reset
     */
    resetIsDirty?: boolean;

    /**
     * Does the editor need to render in edit or view mode (edit by default)
     */
    isReadonly?: boolean;

    /**
     * The message to show when trying to submit an invalid required value
     */
    errorMessage?: string;

    /**
     * The position where the label is shown left | top
     */
    labelPosition?: "left" | "top";

    /**
     * Small description about the editor value
     */
    helperText?: string;

    /**
     * Callback when the value changes
     * @param value - the new value
     */
    onChange?: (value: T) => void;
}
