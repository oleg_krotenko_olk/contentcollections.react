import classNames from "classnames";
import React, { FunctionComponent, ReactElement } from "react";
import { getComponentId } from "Utils";
import { Grid, InputLabel, makeStyles, Tooltip, useTheme } from "@material-ui/core";
import HelpIcon from "@material-ui/icons/Help";
import { grayColorTransparent, whiteColorTransparent } from "ThemeVariables";


// styles
const useStyles = makeStyles(theme => ({
    formWrapperVertical: {
        "marginBottom": theme.spacing(3),
        "&:last-child": {
            marginBottom: 0,
        },
    },

    formHorizontalWrapper: {
        "marginBottom": theme.spacing(2),
        "&:last-child": {
            marginBottom: 0,
        },
    },
    label: {
        display: "flex",
        alignItems: "center",
    },
    rightAlignedLabel: {
        margin: 0,
        lineHeight: 1.5,
        wordBreak: "break-word",
        justifyContent: "flex-end",
    },

    leftAlignedLabelWrapper: {
        "width": 120,
        "marginRight": theme.spacing(2),
        "textAlign": "right",
        "@media (max-width: 400px)": {
            width: "100%",
            textAlign: "left",
            marginRight: 0,
            marginBottom: theme.spacing(0.5),
        },
    },

    leftAlignedLabelWrapperWide: {
        "width": 240,
        "marginRight": theme.spacing(2),
        "textAlign": "right",
        "@media (max-width: 600px)": {
            width: "100%",
            textAlign: "left",
            marginRight: 0,
            marginBottom: theme.spacing(0.5),
        },
    },

    rightAlignedLabelEdit: {
        marginTop: theme.spacing(1.2),
        color: grayColorTransparent[600],
    },

    topAlignedLabel: {
        fontSize: "12px",
        marginBottom: "8px",
    },

    topAlignedLabelEdit: {
        color: grayColorTransparent[600],
    },

    tooltipLeft: {
        marginRight: theme.spacing(1),
    },

    tooltipRight: {
        marginLeft: theme.spacing(1),
    },
    editLabel: {
        color: grayColorTransparent["600"],
    },
    viewLabel: {
        color: grayColorTransparent["500"],
    },
    // dark mode overwrites
    editLabelOnDark: {
        color: `${whiteColorTransparent[700]} !important`,
    },
    viewLabelDark: {
        color: whiteColorTransparent["600"],
    },
}));

// props
export interface LabelProps {
    /**
     * The label to show next to the component
     */
    label: string;

    /**
     * The position where the label is shown left | top
     */
    labelPosition?: "left" | "top";

    /**
     * Indicates if the labels are given some more space (when label position is left)
     */
    useWiderLabels?: boolean;

    /**
     * Indicates if it is required to give a value
     */
    isRequired?: boolean;

    /**
     * Does the editor need to render in edit or view mode (edit by default)
     */
    isReadonly?: boolean;

    /**
     * Custom class name to overrule some styling
     */
    className?: string;

    /**
     * Small description about the editor value
     */
    helperText?: string;
}

// component
const Label: FunctionComponent<LabelProps> = ({
    label,
    labelPosition = "top",
    useWiderLabels = false,
    isRequired = false,
    isReadonly = false,
    className,
    helperText,
    children,
}): ReactElement => {
    const theme = useTheme();
    const darkMode = theme.palette.type === "dark";
    const classes = useStyles(theme);

    // Return the component to render for help text on the requested position
    const getHelpText = (position?: string): ReactElement => {
        if (helperText && !isReadonly && labelPosition === position) {
            return (
                <Tooltip
                    title={helperText}
                    className={labelPosition === "left" ? classes.tooltipLeft : classes.tooltipRight}
                >
                    <HelpIcon fontSize="small" />
                </Tooltip>
            );
        }

        return <></>;
    };

    const getItemClassName = (): string => {
        if (labelPosition === "left") {
            if (useWiderLabels) {
                return classes.leftAlignedLabelWrapperWide;
            }

            return classes.leftAlignedLabelWrapper;
        }

        return "";
    };

    const getInputLabelClassName = (): string => {
        const cssClasses = [classes.label];

        if (isReadonly) {
            if (darkMode) {
                cssClasses.push(classes.viewLabelDark);
            } else {
                cssClasses.push(classes.viewLabel);
            }
        }

        if (labelPosition === "left") {
            cssClasses.push(classes.rightAlignedLabel);

            if (!isReadonly) {
                cssClasses.push(classes.rightAlignedLabelEdit);
            }
        } else {
            cssClasses.push(classes.topAlignedLabel);

            if (!isReadonly) {
                cssClasses.push(classes.topAlignedLabelEdit);
            }
        }

        if (darkMode && !isReadonly) {
            cssClasses.push(classes.editLabelOnDark);
        }

        if (!darkMode && !isReadonly) {
            cssClasses.push(classes.editLabel);
        }

        return classNames(cssClasses);
    };

    const accessibilityId = getComponentId("input");

    return (
        <Grid
            container
            spacing={0}
            className={classNames(
                className,
                labelPosition === "top" ? classes.formWrapperVertical : classes.formHorizontalWrapper
            )}
        >
            <Grid item className={getItemClassName()} xs={labelPosition === "top" ? 12 : undefined}>
                <InputLabel
                    required={isReadonly ? false : isRequired}
                    className={getInputLabelClassName()}
                    htmlFor={accessibilityId}
                >
                    {getHelpText("left")}
                    {label}
                    {getHelpText("top")}
                </InputLabel>
            </Grid>

            {/* make sure label and input point to the same id */}
            {React.Children.map(children as ReactElement, (child: ReactElement) => {
                return React.cloneElement(child, { accessibilityId }, null);
            })}
        </Grid>
    );
};

export default Label;
