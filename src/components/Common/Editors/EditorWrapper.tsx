import React, { FunctionComponent, ReactElement } from "react";

import { FormHelperText, Grid, makeStyles, useTheme } from "@material-ui/core";
import { whiteColorTransparent } from "ThemeVariables";

// styles
const useStyles = makeStyles(() => ({
    fluidContent: {
        flex: 1,
    },

    // dark mode overwrites
    helperTextOnDark: {
        color: `${whiteColorTransparent[700]} !important`,
    },
}));

// props
interface EditorWrapperProps {
    /**
     * Indicates if the editor contains invalid data
     */
    isValid?: boolean;

    /**
     * Does the editor need to render in edit or view mode (edit by default)
     */
    isReadonly?: boolean;

    /**
     * Small description about the editor value
     */
    helperText?: string;

    /**
     * A random id that will be used for accessibility
     * (i.e. "for" attribute on label should have the same id as the input)
     */
    accessibilityId?: string;

    /**
     * The message to show when trying to submit an invalid required value
     */
    errorMessage?: string;

    /**
     * The position where the label is shown left | top
     */
    labelPosition?: "left" | "top";

    children: ReactElement | Array<ReactElement>;
}

// component
const EditorWrapper: FunctionComponent<EditorWrapperProps> = ({
    isValid,
    children,
    helperText,
    isReadonly = false,
    accessibilityId,
    errorMessage,
    labelPosition = "top",
}): ReactElement => {
    const theme = useTheme();
    const darkMode = theme.palette.type === "dark";
    const classes = useStyles(theme);

    // Return the component to render for help text
    const getHelpText = (): ReactElement => {
        if (helperText && !isReadonly) {
            return (
                <FormHelperText className={darkMode ? classes.helperTextOnDark : ""} id={accessibilityId}>
                    {helperText}
                </FormHelperText>
            );
        }

        return <></>;
    };

    // return the component to render error message
    const getErrorMessage = (): null | ReactElement => {
        if (isValid || !errorMessage || isReadonly) {
            return null;
        }

        return <FormHelperText error>{errorMessage}</FormHelperText>;
    };

    return (
        <Grid
            item
            className={labelPosition === "left" ? classes.fluidContent : ""}
            xs={labelPosition === "top" ? 12 : undefined}
        >
            {children}
            {getHelpText()}
            {getErrorMessage()}
        </Grid>
    );
};

export default EditorWrapper;
