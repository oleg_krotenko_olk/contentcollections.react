import classNames from "classnames";
import React, { ReactElement, useEffect, useState } from "react";
import { getUniqueElementId, isFunction } from "Utils";

import { FilledInput, makeStyles, Typography, useTheme } from "@material-ui/core";
import { Variant } from "@material-ui/core/styles/createTypography";
import { inputMaxWidth } from "ThemeVariables";

import { BaseEditorProps } from "Components/Common/Editors/BaseEditorProps";
import EditorWrapper from "Components/Common/Editors/EditorWrapper";

// styles
const useStyles = makeStyles(theme => ({
    text: {
        margin: 0,
    },
    wordBreak: {
        wordBreak: "break-word",
    },
    multiValueItemsWrapper: {
        "display": "flex",
        "flexWrap": "wrap",
        "& > *": {
            marginRight: theme.spacing(1),
            marginBottom: theme.spacing(1),
            textOverflow: "ellipsis",
            maxWidth: "100%",
        },
    },
    chip: {
        textOverflow: "ellipsis",
        maxWidth: "100%",
    },
    chipContainer: { maxWidth: "100%" },
}));

// props
interface SpecificProps {
    /**
     * Indicates the input type of the editor
     */
    type?: "text" | "password";

    /**
     * Indicates if the editor should get focus
     */
    autoFocus?: boolean;

    /**
     * The typography variant that is used to show the text in view mode
     */
    displayTemplate?: ["h1" | "h2" | "h3" | "h4" | "lead" | "micro-copy"];

    /**
     * A regular expression that is used to validate the text input if the character doesn't comply the character is not set
     */
    regex?: RegExp;

    /**
     * Indicates if the textfield should render a multi line text field (text area)
     */
    multiLine?: boolean;

    /**
     * Indicates if the textfield should take up the full width of its parent
     */
    fullWidth?: boolean;

    /**
     * The max width in px of the text field (default is 300px)
     */
    maxWidth?: string;

    /**
     * The min width in px of the text field (default is 300px)
     */
    minWidth?: string;

    /**
     * The placeholder text that is shown when the text field has no value
     */
    placeHolder?: string;

    /**
     * Indicates if the browser may hint on previously typed values in the field (default is off)
     */
    autoComplete?: "on" | "off";

    /**
     * The position where the label is shown left | top
     */
    labelPosition?: "left" | "top";

    /**
     * Callback when the enter key is pressed (single line only)
     */
    onEnter?: (value: string | number) => void;

    /**
     * Callback when any key is pressed
     */
    onKeyPress?: (event: React.KeyboardEvent<HTMLElement>) => void;

    /**
     * Callback when the text field looses focus
     */
    onBlur?: (value: string | number) => void;
}

export type TextEditorProps<T> = BaseEditorProps<T> & SpecificProps;

// component
const TextEditor = <T extends string | number = string>({
    type = "text",
    autoFocus = false,
    displayTemplate,
    regex,
    multiLine = false,
    fullWidth,
    maxWidth: maxWidth_ = `${inputMaxWidth}px`,
    minWidth = "150px",
    placeHolder = "",
    autoComplete = "off",
    labelPosition,
    onBlur: onBlur_,
    onEnter: onEnter_,
    onKeyPress: onKeyPress_,
    value: value_,
    isRequired = false,
    onChange: onChange_,
    resetIsDirty,
    disabled = false,
    id,
    className,
    accessibilityId,
    isReadonly,
    helperText,
    ...props
}: TextEditorProps<T>): ReactElement => {
    const theme = useTheme();
    const classes = useStyles(theme);
    const maxWidth = multiLine || fullWidth ? "none" : maxWidth_;
    const [isDirty, setIsDirty] = useState(false);
    const [isValid, setIsValid] = useState<boolean>(
        !isRequired || !isDirty || (isDirty && isRequired && value_ !== "")
    );
    const [value, setValue] = useState(value_ || "");

    // default read mode typography variant
    // default header variant
    let typographyVariant = "body1";

    if (displayTemplate) {
        if (displayTemplate[0] === "lead") {
            typographyVariant = "h5";
        } else if (displayTemplate[0] === "micro-copy") {
            typographyVariant = "caption";
        } else {
            [typographyVariant] = displayTemplate;
        }
    }

    const onChangeValue = (newValue: string): void => {
        if (typeof value === "number") {
            const numberValue = parseInt(newValue, 10);

            setValue(numberValue as T);

            if (isFunction(onChange_)) {
                onChange_(numberValue as T);
            }
        } else {
            setValue(newValue as T);

            if (isFunction(onChange_)) {
                onChange_(newValue as T);
            }
        }
    };

    // single value text field change
    const onChange = (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>): void => {
        event.stopPropagation();
        setIsDirty(true);

        const tempValue = event.currentTarget.value;

        setIsValid(!isRequired || (isRequired && tempValue !== ""));

        if (regex) {
            const match = regex.exec(tempValue);

            if ((match && match[0].length === tempValue.length) || tempValue === "") {
                onChangeValue(tempValue);
            }

            return;
        }

        onChangeValue(tempValue);
    };

    const onBlur = (): void => {
        if (onBlur_) {
            onBlur_(value);
        }
    };

    const onKeyPress = (event: React.KeyboardEvent<never>): void => {
        if (onEnter_ && event.key === "Enter") {
            event.preventDefault();
            onEnter_(value);
        }

        if (onKeyPress_) {
            onKeyPress_(event);
        }
    };

    useEffect(() => {
        if (resetIsDirty) {
            setIsDirty(false);
            setIsValid(true);
        }
    }, [resetIsDirty]);

    useEffect(() => {
        setValue(value_ ?? "");
    }, [value_]);

    const helpTextAccessibilityId = (helperText && getUniqueElementId("help-text")) || null;

    const getEditor = (): ReactElement => {
        if (isReadonly) {
            return (
                <Typography
                    variant={(typographyVariant as Variant) || "body1"}
                    className={classNames(labelPosition === "left" ? classes.text : null, classes.wordBreak)}
                    key={id}
                    data-testid={id}
                >
                    {value}
                </Typography>
            );
        }

        return (
            <FilledInput
                type={type}
                required={isRequired}
                autoFocus={autoFocus}
                value={value}
                onChange={onChange}
                error={!isValid}
                disabled={disabled}
                onBlur={onBlur}
                autoComplete={type === "password" ? "new-password" : autoComplete}
                fullWidth
                className={classNames(className)}
                placeholder={placeHolder}
                data-testid={id}
                style={{ maxWidth, minWidth }}
                multiline={multiLine}
                onKeyPress={onKeyPress}
                id={accessibilityId}
                aria-describedby={helpTextAccessibilityId || ""}
            />
        );
    };

    return (
        <EditorWrapper
            isValid={isValid}
            isReadonly={isReadonly}
            labelPosition={labelPosition}
            accessibilityId={helpTextAccessibilityId || ""}
            helperText={helperText}
            {...props}
        >
            {getEditor()}
        </EditorWrapper>
    );
};

export default TextEditor;
