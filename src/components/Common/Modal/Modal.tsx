import classNames from "classnames";
import Theme from "ContentHubThemes";
import React, { FunctionComponent, ReactElement } from "react";

import {
    Box,
    Dialog,
    DialogActions,
    DialogContent,
    DialogProps,
    DialogTitle,
    IconButton,
    makeStyles,
    ThemeProvider,
    useMediaQuery,
    useTheme,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

import DrawerModal from "Components/Common/Modal/DrawerModal";
import { OperationProps } from "Components/Common/Operations/Operation";

// styles
const useStyles = makeStyles(theme => ({
    dialogTitleContentWrapper: {
        display: "flex",
        alignItems: "center",
        paddingRight: "30px",
    },
    flex1: {
        flex: 1,
    },
    addButton: {
        marginBottom: 16,
        minWidth: 0,
    },
    closeButton: {
        flip: false,
        position: "absolute",
        right: theme.direction === "ltr" ? theme.spacing(1) : "auto",
        left: theme.direction === "ltr" ? "auto" : theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
    transparentModal: {
        backgroundColor: "rgba(0,0,0,0.67) !important",
    },
    whiteModalHeader: {
        color: "white",
    },
    modalMinHeight: {
        height: "calc(100vh - 200px)",
    },
    titleFix: {
        fontSize: '17.5px !important'
    }
}));

export type ModalType = "Drawer" | "ExtraSmallModal" | "SmallModal" | "MediumModal" | "LargeModal" | "ExtraLargeModal";

export interface ModalProps {
    /**
     * The id of the modal
     */
    id: string;

    /**
     * The array of operations to show on the modal footer
     */
    modalActions?: Array<ReactElement<OperationProps>>;

    /**
     * The array of operations to show in the modal header
     */
    modalHeaderActions?: Array<ReactElement<OperationProps>>;

    /**
     * The title of the modal
     */
    modalTitle?: string;

    /**
     * Indicates if the modal should be visible
     */
    modalVisible?: boolean;

    /**
     * Callback when the modal closes
     */
    onModalClose?: () => void;

    /**
     * Indicates if the modal should show fullscreen
     */
    fullScreen?: boolean;

    /**
     * Indicates if the width should be equal to the maxWidth
     */
    fullWidth?: boolean;

    /**
     * Define the maxWidth of the modal (the strings correspond to breakpoints in the css)
     */
    maxWidth?: DialogProps["maxWidth"];

    /**
     * The scrollbar should be shown on the paper (the modal itself) or on the body of the page
     */
    scroll?: "body" | "paper";

    /**
     * Indicates if the modal should close when clicking the backdrop
     */
    disableBackdropClick?: boolean;

    /**
     * Indicates if the modal should close when pressing the ESCAPE key
     */
    disableEscapeKeyDown?: boolean;

    /**
     * Indicates if the header should not show a button to close the modal
     */
    disableHeaderCloseButton?: boolean;

    /**
     * Indicates if the modal should be transparent (a transparent black background is shown e.g. for preview modals)
     */
    transparent?: boolean;

    /**
     * Render the modals as a drawer (this way a user can still interact with the page and multiple drawer modals will be shown next to eachother)
     */
    isDrawerModus?: boolean;

    /**
     * Optional icon to show on the drawer header
     */
    drawerHeaderIcon?: string;

    /**
     * Modal should be shown with a fixed height
     */
    hasFixedHeight?: boolean;

    /**
     * The type of the modal (maps size and type)
     */
    modalType?: ModalType;
}

const Modal: FunctionComponent<ModalProps> = ({
    id,
    modalActions,
    modalHeaderActions,
    modalTitle,
    modalVisible = false,
    onModalClose,
    fullScreen = false,
    fullWidth = false,
    maxWidth: maxWidth_ = "sm",
    scroll = "paper",
    disableBackdropClick = false,
    disableEscapeKeyDown = false,
    disableHeaderCloseButton = false,
    children,
    transparent,
    isDrawerModus: isDrawerModus_ = false,
    hasFixedHeight = false,
    modalType = "SmallModal",
}): ReactElement => {
    const theme = useTheme();
    const classes = useStyles(Theme);
    const shouldShowFullScreen = useMediaQuery(theme.breakpoints.down("sm"));
    const minHeightModalStyles =
        hasFixedHeight && !shouldShowFullScreen ? { paper: classes.modalMinHeight } : undefined;

    let maxWidth = maxWidth_;
    let isDrawerModus = isDrawerModus_;

    if (modalType === "Drawer") {
        isDrawerModus = true;
    }

    if (modalType === "ExtraSmallModal") {
        maxWidth = "xs";
    }

    if (modalType === "MediumModal") {
        maxWidth = "md";
    }

    if (modalType === "LargeModal") {
        maxWidth = "lg";
    }

    if (modalType === "ExtraLargeModal") {
        maxWidth = "xl";
    }

    return (
        <ThemeProvider theme={Theme}>
            {isDrawerModus && modalVisible ? (
                <DrawerModal
                    id={id}
                    modalActions={modalActions}
                    modalHeaderActions={modalHeaderActions}
                    onModalClose={onModalClose}
                    disableEscapeKeyDown={disableEscapeKeyDown}
                    disableHeaderCloseButton={disableHeaderCloseButton}
                    modalTitle={modalTitle}
                >
                    {children}
                </DrawerModal>
            ) : (
                <Dialog
                    open={modalVisible}
                    onClose={onModalClose}
                    aria-labelledby="form-dialog-title"
                    fullScreen={fullScreen || shouldShowFullScreen}
                    fullWidth={fullWidth}
                    maxWidth={maxWidth}
                    scroll={scroll}
                    disableBackdropClick={disableBackdropClick}
                    disableEscapeKeyDown={disableEscapeKeyDown}
                    data-testid={id}
                    classes={
                        transparent !== undefined
                            ? {
                                  scrollPaper: classes.transparentModal,
                                  paper: classes.transparentModal,
                                  paperScrollPaper: classes.transparentModal,
                              }
                            : minHeightModalStyles
                    }
                >
                    <DialogTitle>
                        <Box className={classes.dialogTitleContentWrapper}>
                            <Box
                                style={{fontSize: "17.5px !important"}}
                                className={classNames(
                                    classes.flex1,
                                    classes.titleFix,
                                    transparent !== undefined ? classes.whiteModalHeader : undefined
                                )}
                            >
                                {modalTitle}
                            </Box>
                            {modalHeaderActions}
                            {disableHeaderCloseButton ? null : (
                                <IconButton
                                    aria-label="close"
                                    className={classes.closeButton}
                                    onClick={e => {
                                        e.stopPropagation();

                                        if (typeof onModalClose === "function") {
                                            onModalClose();
                                        }
                                    }}
                                >
                                    <CloseIcon />
                                </IconButton>
                            )}
                        </Box>
                    </DialogTitle>
                    <DialogContent>{children}</DialogContent>
                    {modalActions?.length ? <DialogActions>{modalActions}</DialogActions> : null}
                </Dialog>
            )}
        </ThemeProvider>
    );
};

export default Modal;
