import classNames from "classnames";
import { DarkTheme } from "ContentHubThemes";
import useMessagesContext from "Contexts/MessagesContext";
import React, { FunctionComponent, ReactElement, useCallback, useEffect, useState } from "react";
import { isFunction } from "Utils";

import { Box, makeStyles, Portal, Slide, ThemeProvider, useTheme } from "@material-ui/core";
import { grayColorTransparent } from "ThemeVariables";

import Operation, { OperationProps } from "Components/Common/Operations/Operation";

const useStyles = makeStyles(theme => ({
    drawerModal: {
        position: "relative",
        backgroundColor: "#fff",
        width: "589px",
        height: "700px",
        boxShadow: "0 14px 28px rgba(0,0,0,.25), 0 1px 2px rgba(0,0,0,.14)",
        overflow: "hidden",
        maxHeight: "80vh",
        maxWidth: "80vw",
        margin: "0 16px 0 0",
        zIndex: 1050,
        pointerEvents: "all",
        borderRadius: "3px",
    },
    drawerModalFullscreen: {
        width: "90vw",
        height: "90vh",
        maxWidth: "90vw",
        maxHeight: "90vh",
        position: "fixed",
        left: "5vw",
        top: "5vh",
    },
    drawerModalCollapsed: {
        height: "auto",
        width: "280px",
        position: "relative",
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
    },
    drawerModalHeader: {
        padding: "4px 8px 4px 24px",
        color: "white",
        backgroundColor: grayColorTransparent[800],
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
    },
    titleText: {
        overflow: "hidden",
        maxWidth: "100%",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
    },
    titleActions: {
        whiteSpace: "nowrap",
    },
    drawerModalContent: {
        padding: "24px",
        position: "absolute",
        top: "46px",
        bottom: "53px",
        left: 0,
        right: 0,
        overflow: "auto",
        zIndex: 1050,
        backgroundColor: "#fff",
    },
    drawerModalFooter: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        padding: "8px 16px",
        borderTop: `1px solid ${grayColorTransparent[200]}`,
        textAlign: "right",
        minHeight: "53px",
    },
    hidden: {
        display: "none",
    },
}));

const defaultMessages = {
    minimize: "Minimize",
    maximize: "Maximize",
    viewLarger: "Full-screen",
    viewSmaller: "Exit full-screen",
    close: "Close",
};

interface DrawerModalProps {
    /**
     * The id of the modal
     */
    id: string;

    /**
     * The array of operations to show on the modal footer
     */
    modalActions?: Array<ReactElement<OperationProps>>;

    /**
     * The array of operations to show in the modal header
     */
    modalHeaderActions?: Array<ReactElement<OperationProps>>;

    /**
     * The title of the modal
     */
    modalTitle?: string;
    /**
     * Indicates if the modal should close when pressing the ESCAPE key
     */
    disableEscapeKeyDown?: boolean;

    /**
     * Indicates if the header should not show a button to close the modal
     */
    disableHeaderCloseButton?: boolean;

    /**
     * Callback when the modal closes
     */
    onModalClose?: () => void;
}

const DrawerModal: FunctionComponent<DrawerModalProps> = ({
    id,
    modalTitle,
    modalActions,
    modalHeaderActions,
    onModalClose,
    disableEscapeKeyDown = false,
    disableHeaderCloseButton = false,
    children,
}): ReactElement => {
    const theme = useTheme();
    const classes = useStyles(theme);
    const container = document.getElementById("drawer-modal-container");
    const messages = useMessagesContext(defaultMessages);
    const [isFullScreen, setIsFullScreen] = useState(false);
    const [isMinimized, setIsMinimized] = useState(false);
    const [drawer, setDrawer] = useState<HTMLElement>();
    const modalRef = useCallback((node: HTMLDivElement) => {
        if (node !== null) {
            setDrawer(node);
        }
    }, []);
    const [focusableModalElements, setFocusableModalElements] = useState<NodeList>();
    const [actions, setActions] = useState<HTMLElement>();
    const modalActionsRef = useCallback((node: HTMLSpanElement) => {
        if (node !== null) {
            setActions(node);
        }
    }, []);

    /**
     * Make the drawer only show the title and header buttons
     */
    const minimizeModal = (): void => {
        setIsMinimized(true);
    };

    /**
     * Restore the modal to its previous state
     */
    const maximizeModal = (): void => {
        setIsMinimized(false);
    };

    /**
     * Make the modal full screen
     */
    const viewLarger = (): void => {
        setIsMinimized(false);
        setIsFullScreen(true);
    };

    /**
     * Restore to the default drawer modal size
     */
    const viewSmaller = (): void => {
        setIsFullScreen(false);
        setIsMinimized(false);
    };

    /**
     * Close the drawer modal
     */
    const closeModal = (): void => {
        if (isFunction(onModalClose)) {
            onModalClose();
        }
    };

    useEffect(() => {
        if (drawer !== null && drawer !== undefined && drawer instanceof HTMLElement) {
            const elements = drawer.querySelectorAll(
                'a[href], button, textarea, input[type="text"], input[type="radio"], input[type="checkbox"], select'
            );

            setFocusableModalElements(elements);
        }
    }, [drawer]);

    useEffect(() => {
        // handle the focus inside the drawer
        const handleTabKey = (e: KeyboardEvent): unknown => {
            if (focusableModalElements && focusableModalElements.length) {
                const firstElement = focusableModalElements[0];
                const lastElement = focusableModalElements[focusableModalElements.length - 1];

                if (!e.shiftKey && document.activeElement === lastElement) {
                    (firstElement as HTMLElement).focus();

                    return e.preventDefault();
                }

                if (e.shiftKey && document.activeElement === firstElement) {
                    (lastElement as HTMLElement).focus();

                    return e.preventDefault();
                }
            }

            return true;
        };

        // make the modal close when the escape key is pressed
        const handleEscKey = (e: KeyboardEvent): void => {
            if (e.keyCode === 27 && !disableEscapeKeyDown) {
                if (typeof onModalClose === "function") {
                    onModalClose();
                }
            }
        };

        const keyListenersMap = new Map([
            [27, handleEscKey],
            [9, handleTabKey],
        ]);

        // map the key events to the correct handlers
        const keyListener = (e: KeyboardEvent): void => {
            const listener = keyListenersMap.get(e.keyCode);

            return listener && listener(e);
        };

        document.addEventListener("keydown", keyListener);

        // put the focus by default on the close button small timeout is needed for the portal to be in place
        setTimeout(() => {
            if (actions !== null && actions !== undefined && actions instanceof HTMLElement) {
                const modalActionsWrapper = actions.querySelectorAll("button");

                const closeButton = modalActionsWrapper[modalActionsWrapper.length - 1];

                closeButton.focus();
            }
        }, 200);

        return () => document.removeEventListener("keydown", keyListener);
    }, [actions, disableEscapeKeyDown, focusableModalElements, modalActionsRef, modalRef, onModalClose]);

    return (
        <Portal container={container}>
            <Slide direction="up" in mountOnEnter unmountOnExit>
                <Box
                    className={classNames(
                        classes.drawerModal,
                        isFullScreen ? classes.drawerModalFullscreen : null,
                        isMinimized ? classes.drawerModalCollapsed : null
                    )}
                    data-testid={id}
                    aria-labelledby="drawer-title"
                    role="dialog"
                >
                    <Box className={classes.drawerModalHeader}>
                        <span className={classes.titleText} id="drawer-title">
                            {modalTitle}
                        </span>
                        <span className={classes.titleActions} ref={modalActionsRef}>
                            <ThemeProvider theme={DarkTheme}>
                                {modalHeaderActions}
                                <Operation
                                    id="minimize-modal"
                                    label={messages.minimize}
                                    displayMode="icon"
                                    isVisible={!isMinimized}
                                    icon="m-icon-line-horizontal-down"
                                    onClick={minimizeModal}
                                    lightButton
                                />
                                <Operation
                                    id="maximize-modal"
                                    label={messages.maximize}
                                    displayMode="icon"
                                    isVisible={isMinimized}
                                    icon="m-icon-line-horizontal-up"
                                    onClick={maximizeModal}
                                    lightButton
                                />
                                <Operation
                                    id="fullscreen-modal"
                                    label={messages.viewLarger}
                                    displayMode="icon"
                                    isVisible={!isFullScreen}
                                    icon="m-icon-resize-larger-2"
                                    onClick={viewLarger}
                                    lightButton
                                />
                                <Operation
                                    id="exit-fullscreen-modal"
                                    label={messages.viewSmaller}
                                    displayMode="icon"
                                    isVisible={isFullScreen}
                                    icon="m-icon-resize-smaller-2"
                                    onClick={viewSmaller}
                                    lightButton
                                />
                                <Operation
                                    id="close-modal"
                                    label={messages.close}
                                    displayMode="icon"
                                    icon="m-icon-cross"
                                    onClick={closeModal}
                                    lightButton
                                    isVisible={!disableHeaderCloseButton}
                                />
                            </ThemeProvider>
                        </span>
                    </Box>

                    <div
                        className={classNames(classes.drawerModalContent, isMinimized ? classes.hidden : null)}
                        ref={modalRef}
                    >
                        {children}
                    </div>
                    <Box className={classNames(classes.drawerModalFooter, isMinimized ? classes.hidden : null)}>
                        {modalActions}
                    </Box>
                </Box>
            </Slide>
        </Portal>
    );
};

export default DrawerModal;
