import Color from "color";
import colorString from "color-string";

import { colors } from "@material-ui/core";

const normalizeFontNames = (fonts: string): string => {
    return fonts
        .replace("! default", "")
        .split(",")
        .map(fontName => `'${fontName}'`)
        .join(",");
};

const normalizeColor = (color: string): string => {
    return colorString.to.rgb(colorString.get.rgb(color) as [number, number, number, number]);
};

const documentStyle = getComputedStyle(document.documentElement);

const getStyle = (className: string): string => {
    return documentStyle.getPropertyValue(className).toString().trim();
};

// convert css variables to javascript variables to be used in the react material UI theme
// colors
export const mainColor = normalizeColor(getStyle("--main-color") || "#171d52");
export const bodyBackgroundColor = normalizeColor(getStyle("--body-bg") || "#F6F6F6");
export const colorWhite = "#ffffff";
export const colorBlack = "#000000";
export const colorRed = colors.red[500];
export const colorGreen = colors.green[500];
export const colorOrange = colors.orange[500];
export const colorBlue = colors.blue[500];
export const colorPink = colors.pink[500];
export const colorPurple = colors.purple[500];
export const colorDeepPurple = colors.purple[500];
export const colorIndigo = colors.indigo[500];
export const colorLightBlue = colors.lightBlue[500];
export const colorCyan = colors.cyan[500];
export const colorTeal = colors.teal[500];
export const colorLightGreen = colors.lightGreen[500];
export const colorLime = colors.lime[500];
export const colorYellow = colors.yellow[500];
export const colorAmber = colors.amber[500];
export const colorDeepOrange = colors.deepOrange[500];
export const colorBrown = colors.brown[500];
export const colorBlueGray = colors.blueGrey[500];
export const colorMainColor = normalizeColor(getStyle("--main-color") || "#171d52");

// typography
// h1
export const fontFamilyH1 =
    normalizeFontNames(getStyle("--font-family-h1")) || "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
export const fontWeightH1 = getStyle("--font-weight-h1") || 600;
export const fontStyleH1 = getStyle("--font-style-h1") || "normal";
export const textCaseH1 = getStyle("--text-case-h1") || "none";
export const fontColorH1 = normalizeColor(getStyle("--font-color-h1") || "rgba(0, 0, 0, 0.87)");
export const fontSizeH1 = getStyle("--font-size-h1") || "45px";

// h2
export const fontFamilyH2 =
    normalizeFontNames(getStyle("--font-family-h2")) || "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
export const fontWeightH2 = getStyle("--font-weight-h2") || 600;
export const fontStyleH2 = getStyle("--font-style-h2") || "normal";
export const textCaseH2 = getStyle("--text-case-h2") || "none";
export const fontColorH2 = normalizeColor(getStyle("--font-color-h2") || "rgba(0, 0, 0, 0.87)");
export const fontSizeH2 = getStyle("--font-size-h2") || "32px";

// h3
export const fontFamilyH3 =
    normalizeFontNames(getStyle("--font-family-h3")) || "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
export const fontWeightH3 = getStyle("--font-weight-h3") || 600;
export const fontStyleH3 = getStyle("--font-style-h3") || "normal";
export const textCaseH3 = getStyle("--text-case-h3") || "none";
export const fontColorH3 = normalizeColor(getStyle("--font-color-h3") || "rgba(0, 0, 0, 0.87)");
export const fontSizeH3 = getStyle("--font-size-h3") || "16px";

// h4
export const fontFamilyH4 =
    normalizeFontNames(getStyle("--font-family-h4")) || "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
export const fontWeightH4 = getStyle("--font-weight-h4") || 600;
export const fontStyleH4 = getStyle("--font-style-h4") || "normal";
export const textCaseH4 = getStyle("--text-case-h4") || "none";
export const fontColorH4 = normalizeColor(getStyle("--font-color-h4") || "rgba(0, 0, 0, 0.87)");
export const fontSizeH4 = getStyle("--font-size-h4") || "14px";
export const fontSizeH5 = getStyle("--font-size-h5") || "13px";

// h5
export const fontFamilyH5 =
    normalizeFontNames(getStyle("--font-family-h5")) || "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
export const fontWeightH5 = getStyle("--font-weight-h5") || 600;
export const fontStyleH5 = getStyle("--font-style-h5") || "normal";
export const textCaseH5 = getStyle("--text-case-h5") || "none";
export const fontColorH5 = normalizeColor(getStyle("--font-color-h5") || "rgba(0, 0, 0, 0.38)");

// base font family
export const fontFamilyBase =
    normalizeFontNames(getStyle("--font-family-base")) || "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";

// monospace font family
export const fontFamilyMonoSpace =
    'SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace';

// default bold font weight
export const fontWeightBold: "normal" | "bold" | "bolder" | "lighter" | number =
    (getStyle("--font-weight-bold") as "normal" | "bold" | "bolder" | "lighter") || 600;
// default normal color
export const fontWeightNormal: "normal" | "bold" | "bolder" | "lighter" | number =
    (getStyle("--font-weight-normal") as "normal" | "bold" | "bolder" | "lighter") || 400;

// buttons
export const btnFontWeight = getStyle("--btn-font-weight") || 600;
export const btnFontFamily =
    normalizeFontNames(getStyle("--btn-font-family")) || "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
export const btnTextTransform = getStyle("--btn-text-transform") || "none";
export const btnBorderRadius = getStyle("--btn-border-radius") || "3px";
export const baseBorderRadius = getStyle("--btn-border-radius") || "3px";

// primary color button
export const btnPrimaryBg = normalizeColor(getStyle("--btn-primary-bg") || "#00908e");
export const btnPrimaryHoverBg = normalizeColor(getStyle("--btn-primary-hover-bg") || "#219e9d");
export const btnPrimaryFocusBg = normalizeColor(getStyle("--btn-primary-focus-bg") || "#219e9d");
export const btnPrimaryActiveBg = normalizeColor(getStyle("--btn-active-bg") || "#42adab");
export const btnPrimaryColor = normalizeColor(getStyle("--btn-primary-color") || "#ffffff");

export const borderRadiusMd = getStyle("--border-radius-md") || 0;

export const borderRadiusSm = getStyle("--border-radius-sm") || "3px";

export const borderRadiusLg = getStyle("--border-radius-lg") || 0;

export const whiteColorTransparent = {
    50: "rgba(255, 255, 255, 0.02)",
    100: "rgba(255, 255, 255, 0.04)",
    200: "rgba(255, 255, 255, 0.07)",
    300: "rgba(255, 255, 255, 0.12)",
    400: "rgba(255, 255, 255, 0.26)",
    500: "rgba(255, 255, 255, 0.38)",
    600: "rgba(255, 255, 255, 0.54)",
    700: "rgba(255, 255, 255, 0.62)",
    800: "rgba(255, 255, 255, 0.74)",
    900: "rgba(255, 255, 255, 0.87)",
};

export const grayColorTransparent = {
    50: "rgba(0, 0, 0, 0.02)",
    100: "rgba(0, 0, 0, 0.04)",
    200: "rgba(0, 0, 0, 0.07)",
    300: "rgba(0, 0, 0, 0.12)",
    400: "rgba(0, 0, 0, 0.26)",
    500: "rgba(0, 0, 0, 0.38)",
    600: "rgba(0, 0, 0, 0.54)",
    700: "rgba(0, 0, 0, 0.62)",
    800: "rgba(0, 0, 0, 0.74)",
    900: "rgba(0, 0, 0, 0.87)",
};

export const iconColorLight = grayColorTransparent[400];

export const textColorDefault = grayColorTransparent[900];
export const textColorLight = grayColorTransparent[600];
export const textColorLightOnDark = whiteColorTransparent[700];
export const textColorDisabled = grayColorTransparent[500];
export const textColorDisabledOnDark = whiteColorTransparent[600];

const primaryColor: Color = Color(mainColor);

export const themePrimary = {
    50: normalizeColor(getStyle("--accent-shade-50") || "#e3f3f3"),
    100: normalizeColor(getStyle("--accent-shade-100") || "#bae1e0"),
    200: normalizeColor(getStyle("--accent-shade-200") || "#8ccdcc"),
    300: normalizeColor(getStyle("--accent-shade-300") || "#59b7b6"),
    400: normalizeColor(getStyle("--accent-shade-400") || "#29a2a0"),
    500: normalizeColor(getStyle("--accent-shade-500") || "#00908e"),
    600: normalizeColor(getStyle("--accent-shade-600") || "#007c7a"),
    700: normalizeColor(getStyle("--accent-shade-700") || "#003e3d"),
    800: normalizeColor(getStyle("--accent-shade-800") || "#003e3d"),
    900: normalizeColor(getStyle("--accent-shade-900") || "#001d1c"),
};

export const themePrimaryTransparent = {
    50: primaryColor.fade(0.98).string(),
    100: primaryColor.fade(0.96).string(),
    200: primaryColor.fade(0.93).string(),
    300: primaryColor.fade(0.88).string(),
    400: primaryColor.fade(0.74).string(),
    500: primaryColor.fade(0.62).string(),
    600: primaryColor.fade(0.46).string(),
    700: primaryColor.fade(0.38).string(),
    800: primaryColor.fade(0.26).string(),
    900: primaryColor.fade(0.13).string(),
};

export const themePrimaryLightened = {
    50: primaryColor.mix(Color("white"), 0.98).toString(),
    100: primaryColor.mix(Color("white"), 0.96).toString(),
    200: primaryColor.mix(Color("white"), 0.93).toString(),
    300: primaryColor.mix(Color("white"), 0.88).toString(),
    400: primaryColor.mix(Color("white"), 0.74).toString(),
    500: primaryColor.mix(Color("white"), 0.62).toString(),
    600: primaryColor.mix(Color("white"), 0.46).toString(),
    700: primaryColor.mix(Color("white"), 0.38).toString(),
    800: primaryColor.mix(Color("white"), 0.26).toString(),
    900: primaryColor.mix(Color("white"), 0.13).toString(),
};

export const focusColor = "#2196f3";
export const contrastColors = {
    "red": {
        shade1: colors.red[50],
        shade2: colors.red[200],
        shade3: colors.red[500],
        shade4: colors.red[600],
        shade5: colors.red[700],
    },
    "pink": {
        shade1: colors.pink[50],
        shade2: colors.pink[200],
        shade3: colors.pink[500],
        shade4: colors.pink[500],
        shade5: colors.pink[700],
    },
    "purple": {
        shade1: colors.purple[50],
        shade2: colors.purple[200],
        shade3: colors.purple[500],
        shade4: colors.purple[500],
        shade5: colors.purple[700],
    },
    "deep-purple": {
        shade1: colors.deepPurple[50],
        shade2: colors.deepPurple[200],
        shade3: colors.deepPurple[500],
        shade4: colors.deepPurple[500],
        shade5: colors.deepPurple[700],
    },
    "indigo": {
        shade1: colors.indigo[50],
        shade2: colors.indigo[200],
        shade3: colors.indigo[500],
        shade4: colors.indigo[500],
        shade5: colors.indigo[700],
    },
    "blue": {
        shade1: colors.blue[50],
        shade2: colors.blue[200],
        shade3: colors.blue[500],
        shade4: colors.blue[600],
        shade5: colors.blue[800],
    },
    "light-blue": {
        shade1: colors.lightBlue[50],
        shade2: colors.lightBlue[200],
        shade3: colors.lightBlue[500],
        shade4: colors.lightBlue[600],
        shade5: colors.lightBlue[800],
    },
    "cyan": {
        shade1: colors.cyan[50],
        shade2: colors.cyan[200],
        shade3: colors.cyan[500],
        shade4: colors.cyan[600],
        shade5: colors.cyan[800],
    },
    "teal": {
        shade1: colors.teal[50],
        shade2: colors.teal[200],
        shade3: colors.teal[500],
        shade4: colors.teal[500],
        shade5: colors.teal[800],
    },
    "green": {
        shade1: colors.green[50],
        shade2: colors.green[200],
        shade3: colors.green[500],
        shade4: colors.green[600],
        shade5: colors.green[800],
    },
    "light-green": {
        shade1: colors.lightGreen[100],
        shade2: colors.lightGreen[300],
        shade3: colors.lightGreen[500],
        shade4: colors.lightGreen[700],
        shade5: colors.lightGreen[900],
    },
    "lime": {
        shade1: colors.lime[100],
        shade2: colors.lime[500],
        shade3: colors.lime[600],
        shade4: colors.lime[700],
        shade5: grayColorTransparent[600],
    },
    "yellow": {
        shade1: colors.yellow[200],
        shade2: colors.yellow[600],
        shade3: colors.yellow[600],
        shade4: colors.yellow[800],
        shade5: grayColorTransparent[600],
    },
    "amber": {
        shade1: colors.amber[100],
        shade2: colors.amber[400],
        shade3: colors.amber[500],
        shade4: colors.amber[700],
        shade5: grayColorTransparent[600],
    },
    "orange": {
        shade1: colors.orange[100],
        shade2: colors.orange[300],
        shade3: colors.orange[500],
        shade4: colors.orange[700],
        shade5: grayColorTransparent[600],
    },
    "deep-orange": {
        shade1: colors.deepOrange[50],
        shade2: colors.deepOrange[200],
        shade3: colors.deepOrange[500],
        shade4: colors.deepOrange[700],
        shade5: colors.deepOrange[800],
    },
    "blue-gray": {
        shade1: colors.blueGrey[50],
        shade2: colors.blueGrey[200],
        shade3: colors.blueGrey[500],
        shade4: colors.blueGrey[500],
        shade5: colors.blueGrey[700],
    },
    "brown": {
        shade1: colors.brown[50],
        shade2: colors.brown[200],
        shade3: colors.brown[500],
        shade4: colors.brown[500],
        shade5: colors.brown[700],
    },
    "gray": {
        shade1: colors.grey[300],
        shade2: grayColorTransparent[400],
        shade3: grayColorTransparent[500],
        shade4: grayColorTransparent[600],
        shade5: grayColorTransparent[600],
    },
    "light-gray": {
        shade1: colors.grey[300],
        shade2: grayColorTransparent[300],
        shade3: grayColorTransparent[300],
        shade4: grayColorTransparent[500],
        shade5: grayColorTransparent[600],
    },
    "black": {
        shade1: colors.grey[300],
        shade2: grayColorTransparent[500],
        shade3: grayColorTransparent[900],
        shade4: grayColorTransparent[900],
        shade5: grayColorTransparent[900],
    },
    "white": {
        shade1: "white",
        shade2: grayColorTransparent[300],
        shade3: grayColorTransparent[300],
        shade4: grayColorTransparent[500],
        shade5: grayColorTransparent[700],
    },
    "theme-color": {
        shade1: themePrimary[50],
        shade2: themePrimary[200],
        shade3: themePrimary[500],
        shade4: themePrimary[500],
        shade5: themePrimary[700],
    },
};

export const buttonPadding = "6px 10px";

// navbar
export const navbarBg = normalizeColor(getStyle("--navbar-bg") || "#ffffff");

export const navbarLinkColor = normalizeColor(getStyle("--navbar-link-color") || "rgba(0,0,0,.54)");

export const navbarLinkHoverColor = normalizeColor(getStyle("--navbar-link-hover-color") || "rgba(0,0,0,.87)");

export const navbarLinkHoverBg = normalizeColor(getStyle("--navbar-link-hover-bg") || "rgba(0,0,0,0.0)");

export const navbarLinkActiveColor = normalizeColor(getStyle("--navbar-link-active-color") || "rgba(0,0,0,0.87)");

export const navbarLinkActiveBg = normalizeColor(getStyle("--navbar-link-active-bg") || "rgba(0,0,0,0.0)");

export const navbarFontFamily = normalizeFontNames(getStyle("--navbar-font-family") || "'Open Sans'");

export const navbarFontWeight = getStyle("--navbar-font-weight") || 600;

export const navbarTextTransform = getStyle("--navbar-text-transform") || "none";

export const navbarLogoHeight = getStyle("--navbar-logo-height") || "24px";

export const navbarLogoWidth = getStyle("--navbar-logo-width") || "24px";

export const navbarHeight = 40;

// page header
export const pageHeaderBg = normalizeColor(getStyle("--page-header-bg") || "white");

export const pageHeaderColor = normalizeColor(getStyle("--page-header-color") || grayColorTransparent["900"]);

export const pageHeaderFont =
    normalizeFontNames(getStyle("--page-header-font")) ||
    "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif !default";

export const pageHeaderFontWeight = getStyle("--page-header-font-weight") || 600;

export const pageHeaderTextTransform = getStyle("--page-header-text-transform") || "none";

export const pageHeaderDarkMode = getStyle("--page-header-dark-mode") === "true";
export const pageHeaderHeight = 24 * 1.3 + 24;
export const pageHeaderFontSize = getStyle("--page-title-font-size") || "22px";

export const containerPadding = "24px";

// sidebar
export const sidebarWidth = 378;
export const wideSidebarWidth = 560;

// input
export const inputMaxWidth = 300;

export const snackbarStyles = {
    success: { backgroundColor: colorGreen },
    error: { backgroundColor: colorRed },
    warning: { backgroundColor: colorOrange },
    info: { backgroundColor: colorBlue },
};

export const searchGridItemSize = {
    large: 180,
    default: 148,
    small: 100,
};

export const fontSizeSmall = "12px";

export const fontSizeBase = 14;

export const borderColorDark = grayColorTransparent[300];
export const borderColorDarkAbs = colors.grey[300];

export const iconSizeSmall = "20px";

export const gridImageHeight = 150;

export const imageWrapperBg = colors.grey[900];
