import { colors, createMuiTheme, Theme as MuiTheme, ThemeOptions } from "@material-ui/core";
import { Shadows } from "@material-ui/core/styles/shadows";
import {
    baseBorderRadius,
    borderRadiusSm,
    btnBorderRadius,
    btnFontFamily,
    btnFontWeight,
    btnTextTransform,
    buttonPadding,
    colorRed,
    colorWhite,
    focusColor,
    fontColorH1,
    fontColorH2,
    fontColorH3,
    fontColorH4,
    fontColorH5,
    fontFamilyBase,
    fontFamilyH1,
    fontFamilyH2,
    fontFamilyH3,
    fontFamilyH4,
    fontFamilyH5,
    fontSizeBase,
    fontSizeH1,
    fontSizeH2,
    fontSizeH3,
    fontSizeH4,
    fontSizeH5,
    fontSizeSmall,
    fontStyleH1,
    fontStyleH2,
    fontStyleH3,
    fontStyleH4,
    fontStyleH5,
    fontWeightBold,
    fontWeightH1,
    fontWeightH2,
    fontWeightH3,
    fontWeightH4,
    fontWeightH5,
    fontWeightNormal,
    grayColorTransparent,
    iconColorLight,
    mainColor,
    themePrimaryTransparent,
    whiteColorTransparent,
} from "ThemeVariables";

// in this file we override the default material ui theme
// we map the css variables to javascript variables
const documentDirection = document.dir;

const commonThemeStyles = {
    shadows: [
        "none",
        "0px 2px 1px -1px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 1px 3px 0px rgba(0,0,0,0.07)",
        "0px 3px 1px -2px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 1px 5px 0px rgba(0,0,0,0.07)",
        "0px 3px 3px -2px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 1px 8px 0px rgba(0,0,0,0.07)",
        "0px 2px 4px -1px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 1px 10px 0px rgba(0,0,0,0.07)",
        "0px 3px 5px -1px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 1px 14px 0px rgba(0,0,0,0.07)",
        "0px 3px 5px -1px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 1px 18px 0px rgba(0,0,0,0.07)",
        "0px 4px 5px -2px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 2px 16px 1px rgba(0,0,0,0.07)",
        "0px 5px 5px -3px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 3px 14px 2px rgba(0,0,0,0.07)",
        "0px 5px 6px -3px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 3px 16px 2px rgba(0,0,0,0.07)",
        "0px 6px 6px -3px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 4px 18px 3px rgba(0,0,0,0.07)",
        "0px 6px 7px -4px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 4px 20px 3px rgba(0,0,0,0.07)",
        "0px 7px 8px -4px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 5px 22px 4px rgba(0,0,0,0.07)",
        "0px 7px 8px -4px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 5px 24px 4px rgba(0,0,0,0.07)",
        "0px 7px 9px -4px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 5px 26px 4px rgba(0,0,0,0.07)",
        "0px 8px 9px -5px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 6px 28px 5px rgba(0,0,0,0.07)",
        "0px 8px 10px -5px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 6px 30px 5px rgba(0,0,0,0.07)",
        "0px 8px 11px -5px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 6px 32px 5px rgba(0,0,0,0.07)",
        "0px 9px 11px -5px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 7px 34px 6px rgba(0,0,0,0.07)",
        "0px 9px 12px -6px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 7px 36px 6px rgba(0,0,0,0.07)",
        "0px 10px 13px -6px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 8px 38px 7px rgba(0,0,0,0.07)",
        "0px 10px 13px -6px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 8px 40px 7px rgba(0,0,0,0.07)",
        "0px 10px 14px -6px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 8px 42px 7px rgba(0,0,0,0.07)",
        "0px 11px 14px -7px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 9px 44px 8px rgba(0,0,0,0.07)",
        "0px 11px 15px -7px rgba(0,0,0,0.12),0 0 0 1px rgba(0,0,0,.07),0px 9px 46px 8px rgba(0,0,0,0.07)",
    ],
    typography: {
        fontSize: fontSizeBase,
        fontFamily: fontFamilyBase,
        fontWeightLight: fontWeightNormal,
        fontWeightRegular: fontWeightNormal,
        fontWeightMedium: fontWeightBold,
        fontWeightBold,
    },
    MuiTypography: {
        h1: {
            fontFamily: fontFamilyH1,
            fontWeight: fontWeightH1,
            fontStyle: fontStyleH1,
            color: fontColorH1,
            fontSize: fontSizeH1,
            marginBottom: "1em",
            lineHeight: 1.25,
        },

        h2: {
            fontFamily: fontFamilyH2,
            fontWeight: fontWeightH2,
            fontStyle: fontStyleH2,
            color: fontColorH2,
            fontSize: fontSizeH2,
            marginBottom: "1em",
            lineHeight: 1.25,
        },

        h3: {
            fontFamily: fontFamilyH3,
            fontWeight: fontWeightH3,
            fontStyle: fontStyleH3,
            color: fontColorH3,
            fontSize: fontSizeH3,
            marginBottom: "1em",
            lineHeight: 1.25,
        },

        h4: {
            fontFamily: fontFamilyH4,
            fontWeight: fontWeightH4,
            fontStyle: fontStyleH4,
            color: fontColorH4,
            fontSize: fontSizeH4,
            marginBottom: "1em",
            lineHeight: 1.25,
        },

        h5: {
            fontFamily: fontFamilyH5,
            fontWeight: fontWeightH5,
            fontStyle: fontStyleH5,
            color: fontColorH5,
            fontSize: fontSizeH5,
            marginBottom: "1em",
            lineHeight: 1.25,
        },
        subtitle1: {
            fontFamily: fontFamilyBase,
        },
        subtitle2: {
            fontFamily: fontFamilyBase,
        },
        body1: {
            fontFamily: fontFamilyBase,
        },
        body2: {
            fontFamily: fontFamilyBase,
            color: grayColorTransparent[500],
        },
        caption: {
            fontFamily: fontFamilyBase,
            color: grayColorTransparent["500"],
        },
        overline: {
            fontFamily: fontFamilyBase,
        },
    },
    MuiButton: {
        root: {
            borderRadius: btnBorderRadius,
            fontSize: "14px",
            padding: buttonPadding,
            whiteSpace: "nowrap",
        },
        text: {
            padding: buttonPadding,
            color: grayColorTransparent[600],
        },
        outlined: {
            padding: "5px 10px",
        },
        label: {
            fontFamily: btnFontFamily,
            fontWeight: btnFontWeight,
            textTransform: btnTextTransform,
        },
    },
    MuiFilledInput: {
        input: {
            padding: "0 !important",
            lineHeight: "1.2em",
        },
        inputMultiline: {
            padding: 0,
            minHeight: 100,
        },
        multiline: {
            padding: "12px !important",
        },
        underline: {
            "&:after": {
                display: "none",
            },
            "&:before": {
                display: "none",
            },
        },
    },
    MuiFormHelperText: {
        root: {
            fontSize: ".85rem",
            lineHeight: "1.2em",
        },
    },
    MuiTooltip: {
        tooltip: {
            fontSize: ".85rem",
        },
    },
    MuiExpansionPanelSummary: {
        content: {
            margin: "24px 0 !important",
        },
    },
    MuiPaper: {
        root: {
            borderRadius: `${borderRadiusSm} !important`,
            boxShadow: "0 0 0 1px rgba(0,0,0,.12)",
        },
    },
    MuiExpansionPanelDetails: {
        root: {
            paddingTop: `${0} !important`,
        },
    },
    MuiTab: {
        root: {
            fontSize: "14px !important",
            minWidth: "0 !important",
            textTransform: btnTextTransform,
        },
        labelIcon: {
            "fontSize": `${fontSizeSmall} !important`,
            "min-height": "0 !important",
            "& svg": {
                marginBottom: `0 !important`,
            },
        },
    },
    MuiTabPanel: {
        root: {
            paddingLeft: 0,
            paddingRight: 0,
        },
    },
    MuiMenuItem: {
        root: {
            paddingRight: "48px",
        },
    },
    MuiTableCell: {
        root: {
            fontSize: "14px",
            padding: "10px",
            lineHeight: "1.6",
            borderBottomColor: colors.grey[200],
        },
        head: {
            fontSize: 12,
            color: grayColorTransparent[500],
            whiteSpace: "nowrap",
            minHeight: "20px",
        },
    },
    MuiCircularProgress: {
        colorPrimary: {
            color: iconColorLight,
        },
    },
};

const options = {
    direction: documentDirection,
    palette: {
        primary: {
            main: mainColor,
        },
    },
    shadows: commonThemeStyles.shadows as Shadows,
    typography: commonThemeStyles.typography,
    overrides: {
        MuiTypography: commonThemeStyles.MuiTypography,
        MuiButton: {
            ...commonThemeStyles.MuiButton,
            outlinedPrimary: {
                borderColor: themePrimaryTransparent[500],
            },
        },
        MuiIconButton: {
            root: {
                "&$disabled": {
                    backgroundColor: `${grayColorTransparent[100]} !important`,
                },
            },
        },
        MuiFilledInput: {
            root: {
                "backgroundColor": grayColorTransparent["100"],
                "border": `1px solid transparent`,
                "borderRadius": `${baseBorderRadius} !important`,
                "padding": "9px",
                "minHeight": "36px",
                "&:hover": {
                    backgroundColor: grayColorTransparent["200"],
                    border: `1px solid transparent`,
                },
                "&$focused": {
                    backgroundColor: "transparent !important",
                    border: `1px solid ${focusColor}`,
                },

                "&$error": {
                    backgroundColor: "transparent !important",
                    border: `1px solid ${colorRed}`,
                },
                "&::placeholder": {
                    color: "red !important",
                },
            },
            ...commonThemeStyles.MuiFilledInput,
        },
        MuiFormHelperText: commonThemeStyles.MuiFormHelperText,
        MuiTooltip: commonThemeStyles.MuiTooltip,
        MuiInputLabel: {
            root: {
                fontWeight: fontWeightBold,
                color: grayColorTransparent[600],
            },
            asterisk: {
                color: colorRed,
            },
        },
        MuiExpansionPanelSummary: commonThemeStyles.MuiExpansionPanelSummary,
        MuiPaper: commonThemeStyles.MuiPaper,
        MuiExpansionPanelDetails: commonThemeStyles.MuiExpansionPanelDetails,
        MuiTab: commonThemeStyles.MuiTab,
        MuiTabs: {
            root: {
                borderBottom: `1px solid ${grayColorTransparent["300"]}`,
            },
        },
        MuiTabPanel: commonThemeStyles.MuiTabPanel,
        MuiMenuItem: commonThemeStyles.MuiMenuItem,
        MuiTableCell: commonThemeStyles.MuiTableCell,
        MuiCircularProgress: commonThemeStyles.MuiCircularProgress,
        MuiAutocomplete: {
            inputRoot: {
                backgroundColor: grayColorTransparent[100],
                paddingLeft: "4px !important",
                paddingTop: "4px !important",
                paddingBottom: "4px !important",
            },
            tag: {
                "borderRadius": "3px",
                "&:active": {
                    backgroundColor: grayColorTransparent[300],
                },
            },
            input: {
                paddingLeft: "4px !important",
            },
        },
    },
};

const Theme: MuiTheme = createMuiTheme(options as ThemeOptions);

const darkThemeOptions = {
    direction: documentDirection,
    palette: {
        primary: {
            main: mainColor,
        },
        type: "dark",
    },
    shadows: commonThemeStyles.shadows as Shadows,
    typography: commonThemeStyles.typography,
    MuiIconButton: {
        root: {
            "&$disabled": {
                backgroundColor: `${whiteColorTransparent[100]} !important`,
            },
        },
    },
    overrides: {
        MuiTypography: commonThemeStyles.MuiTypography,
        MuiButton: commonThemeStyles.MuiButton,
        MuiFilledInput: {
            root: {
                "backgroundColor": whiteColorTransparent["400"],
                "border": `1px solid transparent`,
                "borderRadius": `${baseBorderRadius} !important`,
                "padding": "9px",
                "minHeight": "36px",
                "&:hover": {
                    backgroundColor: whiteColorTransparent["400"],
                    border: `1px solid transparent`,
                },
                "&$focused": {
                    backgroundColor: "transparent !important",
                    border: `1px solid ${colorWhite}`,
                },
            },
            ...commonThemeStyles.MuiFilledInput,
        },
        MuiFormHelperText: commonThemeStyles.MuiFormHelperText,
        MuiTooltip: commonThemeStyles.MuiTooltip,
        MuiInputLabel: {
            root: {
                fontWeight: fontWeightBold,
                color: whiteColorTransparent[600],
            },
            asterisk: {
                color: colorRed,
            },
        },
        MuiExpansionPanelSummary: commonThemeStyles.MuiExpansionPanelSummary,
        MuiPaper: commonThemeStyles.MuiPaper,
        MuiExpansionPanelDetails: commonThemeStyles.MuiExpansionPanelDetails,
        MuiTab: commonThemeStyles.MuiTab,
        MuiOutlinedInput: {
            notchedOutline: {
                borderColor: "white !important",
            },
        },
        MuiTabs: {
            root: {
                borderBottom: `1px solid ${whiteColorTransparent["300"]}`,
            },
        },
        MuiTabPanel: commonThemeStyles.MuiTabPanel,
        MuiMenuItem: commonThemeStyles.MuiMenuItem,
        MuiTableCell: commonThemeStyles.MuiTableCell,
        MuiCircularProgress: commonThemeStyles.MuiCircularProgress,
        MuiAutocomplete: {
            inputRoot: {
                "backgroundColor": whiteColorTransparent[400],
                "paddingLeft": "4px !important",
                "paddingTop": "4px !important",
                "paddingBottom": "4px !important",

                "&:hover": {
                    backgroundColor: whiteColorTransparent[500],
                },

                "&:focus-within, &:focus": {
                    backgroundColor: "transparent !important",
                    borderColor: "white",
                },
            },
            tagSizeSmall: {
                "borderRadius": "3px",
                "color": "white",
                "backgroundColor": whiteColorTransparent[400],

                "&:active": {
                    backgroundColor: whiteColorTransparent[500],
                },
            },
            input: {
                paddingLeft: "4px !important",
            },
        },
    },
};

export default Theme;
export const DarkTheme: MuiTheme = createMuiTheme(darkThemeOptions as ThemeOptions);
